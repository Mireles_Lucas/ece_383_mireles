----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/16/2017 12:33:35 PM
-- Design Name: 
-- Module Name: lab2_FSM_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use work.lab2Parts.all;	
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab2_FSM_tb is
--  Port ( );
end lab2_FSM_tb;

architecture Behavioral of lab2_FSM_tb is

    signal clk : STD_LOGIC := '0';
    signal reset_n : STD_LOGIC := '1';
    signal sw, cw : STD_LOGIC_VECTOR(2 downto 0) := (others => '0');
    
    type state_type is (Wait4Trig, Wait4Ready, Write, Full);
    signal state: state_type := Wait4Trig;
    
    -- Clock period definitions
    constant clk_period : time := 10 ns;  -- Sets clock to ~ 100MHz

begin

    uut: lab2_fsm 
    port map (clk, reset_n, sw, cw);
    
       -- Clock process definitions
     clk_process :process
     begin
          clk <= '0';
          wait for clk_period/2;
          clk <= '1';
          wait for clk_period/2;
     end process;
     
     reset_n <= '1' after 10 ns;
     
     sw(2) <= '1' after 1 us;

     
     sw(1) <= '1' after 2 ns;
     
     sw(0) <= '1' after 3 ns;
     
     


end Behavioral;
