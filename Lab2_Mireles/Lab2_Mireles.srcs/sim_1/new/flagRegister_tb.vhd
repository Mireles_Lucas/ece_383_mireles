----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	flagRegister_tb.vhd
-- HW:		-
-- Pupr:	test bench for flag register 
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.lab2Parts.all;	

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity flagRegister_tb is
--  Port ( );
end flagRegister_tb;

architecture Behavioral of flagRegister_tb is

signal clk, reset_n : STD_LOGIC := '0';
signal set, clear, Q : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
constant clk_period : time := 10 ns;

begin
uut : flagRegister
    Generic map(8)
    Port map(	
        clk => clk,
        reset_n => reset_n,
        set => set,
        clear => clear,
        Q => Q);

-- Clock process definitions
clk_process :process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
end process;

reset_n <= '1' after 10 ns;

set <= x"02" after 30 ns, x"05" after 50 ns;
clear <= x"02" after 70 ns, x"05" after 90ns;


end Behavioral;
