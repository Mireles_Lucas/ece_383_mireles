----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	lab2_fsm.vhd
-- HW:		-
-- Pupr:	The FSM for lab 2
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.lab2Parts.all;	
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab2_fsm is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
			  sw: in std_logic_vector(2 downto 0);
			  cw: out std_logic_vector (2 downto 0) );
end;

architecture Behavioral of lab2_fsm is

type state_type is (Wait4Trig, Wait4Ready, Write, Full);
signal state: state_type := Wait4Trig;

begin
--              CW
--  State   WREN   Counter
--  1       0       10
--  2       0       10
--  3       1       01
--  4       0       11
--   sw(0) says that we ned to reset counter because it is full
--   sw(1) is the ready signal
--   sw(2) is the trigger
    process(clk)
    begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                state <= Wait4Trig;
            else
                case state is
                    when Wait4Trig =>
                        cw <= "010";
                        if (sw(2) = '1') then state <= Wait4Ready; end if;
                    when Wait4Ready =>
                        cw <= "010";
                        if (sw(1) = '1') then state <= Write; end if;
                    when Write =>
                        cw <= "101";
                        if (sw(0) = '1') then state <= Full;
                        else
                             state <= Wait4Ready; 
                             end if;
                    when Full =>
                        cw <= "011"; --reset the counter
                        state <= Wait4Trig;
                    end case;
                    end if;
                    end if;
                    end process;    
  
end Behavioral;
