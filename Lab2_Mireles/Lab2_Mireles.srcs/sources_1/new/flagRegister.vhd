----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	flagRegister.vhd
-- HW:		-
-- Pupr:	The implementation of Flag Register
--
-- Documentation:	C2C hanson helped me develop the idea to use an intenger to count each bit
-- in order to set and clear individual bits
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity flagRegister is

	Generic (N: integer := 8);
	Port(	clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			set, clear: in std_logic_vector(N-1 downto 0);
			Q: out std_logic_vector(N-1 downto 0));

end flagRegister;

architecture Behavioral of flagRegister is

signal Process_Q : std_logic_vector(N-1 downto 0);


begin

process(clk)         
begin
    if (rising_edge(clk)) then
        for i in 0 to N-1 loop
        if (reset_n = '0') then 
            Q <= (others => '0');
        elsif(set(i) = '1' and clear(i) = '0') then
            Q(i) <= '1';
        elsif(set(i) = '0' and clear(i) = '1') then
            Q(i) <= '0';
        end if;
        end loop;
        end if;
        end process;
end Behavioral;
