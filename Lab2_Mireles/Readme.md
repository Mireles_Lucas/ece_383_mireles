# Lab 2 - Data Acquisition

## By C2C Lucas Mireles

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Block Diagram](#block-diagram)
3. [Implementation](#implementation)
4. [Debug](#debug)
5. [Test and Verification](#test-and-verification)
6. [Conclusion](#conclusion)
7. [Documentation](#documentation)

##### Objectives 
In this lab, we will integrate the video display controller developed in Lab 1 with the audio codec on the ATLYS board to build a basic 2-channel oscilloscope. When complete, your lab should generate an output similar to the picture below.
![](Gate_Check_Images/scope.JPG)

#####Block Diagram
The image below represents the block diagram and all of its entities for the oscilloscope.
![](Gate_Check_Images/block.JPG)

Within this block diagram, one of the biggest components noticed is the datapath.  The datapath includes other components such as the BRAM, Audio Codec Wrapper, the video, and the flag Register.  In addition, it handles multiple processes like the counter to decide which address to write from along with comparing the Bus_in to decide whether or not to trigger. The BRAM is a component that stores memory that can be written to and read from. The FSM or control unit, it a component that actually controls the BRAM and tells it when to write, when to restart, and when to hold. This is decided based on the status word which is shared within the datapath.

##### Debugging 
One of the biggest issues that I ran into with this lab was not being able to get sine wave to trigger correctly. After checking my test bench, I realized that my unsigned signal that I was comparing was in the 200,000s instead of in the 100s.  I then realized that I was comaparing the unadjusted unsigned signal that did not take into consideration that math that was being done to convert it from signed to unsigned. Once this was complete, the sine wave then began to trigger correctly. 

##### Testing and Verification 

###### Prelab - Gate Check 1

By COB Lesson 13, you must have started a Lab 2 Vivado project and downloaded the template files and drop in your Video, VGA, Scopeface, dvid, and tdms files from Lab 1 into your Lab 2 project in order to test your Lab 1 Scopeface works when you implement you Audio Code Wrapper.  Notice from the block diagram…you will copy your Video instantiation and button processes from Lab 1 into your Lab 2 Datapath.  You will also have to re-implement the Lab 1 Clocking Wizard in you Lab 2 project.  Doing this will eliminate a lot of errors from un-driven output signals on lab 2 top. 
Next, you will need to have implement another Clocking Wizard and the Audio Codec Wrapper inside the Datapath entity to get your Audio Codec to begin functioning. Once you fully implement the Audio Codec Wrapper, you will drop in the Loopback process and make connections to loopback the serial ADC input back out to the DAC output (i.e. send the signal back into the Codec). Once you implement the design on the board, you can verify functionality by applying an audio signal to the audio line in jack (blue) and listening to it on the audio line out jack (Green) using a standard oscilloscope. Additionally your Scopeface and Button inputs from Lab 1 should be functional as well.

![](Gate_Check_Images/functionality.jpg)
#######Figure 1 - All of lab one still works along with the loop back audio on the board.

###### Gate Check 2

NOTE: THIS IS THE HARDEST PART! By BOC Lesson 15, you must have implemented and connected the left channel BRAM and BRAM Address Counter to write Audio Codec data to BRAM. Once implemented, you can verify your BRAM works by using the given datapath testbench and watching the BRAM write address increment and data be written/read from the BRAM.
Once this is working, you must implement Video entity (from Lab 1) to take the left channel output from BRAM and send it to the Channel 1 waveform to be displayed when the readL value equals the row value. Once implemented, this functionality can be verified first with the given datapath testbench to verify the channel 1 values are being updated properly when readL equals the row value. Additionally, you may try to implement this on the hardware and verify that your scopeface is still present and some values are being displayed for Channel 1 (at this point the waveform will be scrolling across the display or may be scaled wrong).

![](Gate_Check_Images/GC2.JPG)

#######Figure 2- The WRADDR_sig shows the BRAM address incrementing and the DI and ReadL signal shows data being written to and from the BRAM. Because ReadL and Row do not equal each other, CH1_wave is not high.  Additionally, we can see the counter restart at the value 23 once the counter has reached 1023 and the cw is set to "011".

###### Functionality
Get a single channel of the oscilloscope to display with reliable triggering that holds the waveform at a single point on the left edge of the display. A 220Hz waveform should display something similar to what is shown in the screenshot at the top of this page. Additionally, you must have the following done:

Use a package file to contain all your component declarations.

Use separate datapath and control unit.
Your datapath must use processes which are similar to our basic building block (counter, register, mux, etc.). I do not want to see one massive process that attempts to do all the work in the datapath.

Testbench for the flagRegister.

Testbench for the control unit.

Testbench for the datapath unit showing data (different value than what is given in the testbench) coming out of the audio codec and being converted from signed to unsigned and then to std_logic_vector to go into your BRAM. Include calculations to back up what the waveform shows.

Add a second channel (in green).
Integrate the button debouncing strategy in HW #7 to debounce the buttons controlling the trigger time and trigger voltage.
Move the cursors on the screen.

Use the trigger voltage marker to establish the actual trigger voltage used to capture the waveform. As the trigger is moved up and down, you should see the point at which the waveform intersects the left side of the screen change.

![](Gate_Check_Images/FR_tb.JPG)
####### Figure 3 - test bench for the Flag Register 
![](Gate_Check_Images/FSM_tb.JPG)
####### Figure 4 - test bench for the Flag Register 

![](Gate_Check_Images/GC2.JPG)
####### Figure 5 - test bench for the Datapath

![](Gate_Check_Images/scope.JPG)
####### Figure 6 - Finished product with all functionality met

#####Capability 
###### -Well you have built a oscilloscope, what are its capabilities?

The horizontal axis represents time. There are 10 major divisions on the display; how long does each major division represent? - 1.25 ms

Each major time division is split into 4 minor division, how long does each minor division represent? - .3125 ms

Generate a sine wave that can be fully captured on your display (like the yellow channel in the image at the top of this web page). record its height in major and minor vertical divisions. Measure this same audio output using the break out audio cable. Record the peak-to-peak voltage. Compute the number of volts in each major and minor vertical division.

P-P - 4.5 divisions 
O-scope P-P 0.8325 V

.185 V/divison
37 mV / divison 

Starting at address 0, how long does it take to fill the entire memory with audio samples (coming in at 48kHz)? about 21.33 ms

How long does it take to completely draw the display once?  16.8 ms

The question is likely relevant to Lab 3 - how long is the vsynch signal held low? - 0.06 ms


##### Conclusion 
From this lab, I learned how to utilize the Nexys Board and actually produce a working oscilloscope. The most successful task in the lab was being able to correctly trigger the input of the audio signal. It combined all the homeworks and previous labs to come together and complete a bigger objective.

The only thing I would change for this lab is to make it more obvious that we need a working flag register.

##### Documentation: C2C Hayden and I worked on the question 3 capability together 