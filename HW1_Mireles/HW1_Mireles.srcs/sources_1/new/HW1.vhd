----------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	HW1.vhd
-- HW:		HW1
-- Pupr:	Test bench for HW1  
--
-- Documentation:	Based this off a some previous labs and consulted 
--					page 36 of our text for some useful syntax.	
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity HW1 is
    Port ( I3 : in STD_LOGIC;
           I2 : in STD_LOGIC;
           I1 : in STD_LOGIC;
           I0 : in STD_LOGIC;
           O0 : out STD_LOGIC;
           O1 : out STD_LOGIC);
end HW1;

architecture Behavioral of HW1 is

begin
    O0 <= I3 or (I1 and I3);
    O1 <= I2 or I3;
    


end Behavioral;
