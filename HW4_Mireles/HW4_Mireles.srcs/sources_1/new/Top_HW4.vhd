----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	Top_HW4.vhd
-- HW:		HW4
-- Pupr:	top level for homework 4
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Top_HW4 is
    Port ( Clk : in STD_LOGIC;
           Reset : in STD_LOGIC;
           Ctrl : in STD_LOGIC;
           Q0 : out unsigned(2 downto 0);
           Q1 : out unsigned (2 downto 0));
end Top_HW4;



architecture Behavioral of Top_HW4 is

--signal clk_sig : STD_LOGIC;
--signal reset_sig : STD_LOGIC;
--signal ctrl_sig : STD_LOGIC;
signal Q0_sig : unsigned(2 downto 0);
signal Q1_sig : unsigned (2 downto 0);
signal roll_sig: std_logic;
signal roll2_sig: std_logic;

component Counter
port(
           clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           ctrl : in STD_LOGIC;
           Q : out unsigned (2 downto 0);
           roll : out STD_LOGIC);
end component;
begin

    counter_1: Counter
    port map(
               clk => Clk,
               reset => Reset,
               ctrl => Ctrl,
               Q => Q0_sig,
               roll => roll_sig);
    
    
    counter_2: Counter
    port map(
               clk => Clk,
               reset => Reset,
               ctrl => roll_sig,
               Q => Q1_sig,
               roll => roll2_sig);
     
    Q0 <= Q0_sig;
    Q1 <= Q1_sig;
               
    
           

end Behavioral;
