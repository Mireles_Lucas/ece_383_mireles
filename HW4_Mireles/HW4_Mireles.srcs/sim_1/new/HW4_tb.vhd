----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	HW4_tb.vhd
-- HW:		HW4
-- Pupr:	testbench for counter
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity HW4_tb is
end HW4_tb;

architecture Behavioral of HW4_tb is

component Counter
port(
           clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           ctrl : in STD_LOGIC;
           Q : out unsigned (2 downto 0);
           roll : out STD_LOGIC);
end component;

-- declare signals 
signal clk_sig: std_logic := '0';
signal reset_sig: std_logic := '0';
signal ctrl_sig : STD_LOGIC := '0';
signal Q_sig : unsigned (2 downto 0);
signal roll_sig : STD_LOGIC;

constant clk_period : time := 500 ns;
begin

        UUT: Counter port map (clk_sig, reset_sig, ctrl_sig, Q_sig, roll_sig);
    -- Clock process definitions
       clk_process :process
       begin
            clk_sig <= '0';
            wait for clk_period/2;
            clk_sig <= '1';
            wait for clk_period/2;
       end process;
       
       ctrl_sig <= '1', '1' after 15us, '0' after 16us, '1' after 18us;
       reset_sig <= '0', '1' after 1us;
end;
