----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	lab4_fsm.vhd
-- HW:		-
-- Pupr:	The FSM for lab 4
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use work.lab2Parts.all;	
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values 
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Lab4_fsm is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           ready: in std_logic;
           btn: in	STD_LOGIC_VECTOR(3 downto 0);
		   sw: in std_logic_vector(4 downto 0);
		   cw: out std_logic_vector (2 downto 0) );
end;

architecture Behavioral of Lab4_fsm is

type state_type is (reset, wait4value, Inc_freq, Dec_Freq, Get_Base, Get_Base_plus, Math);
signal state: state_type := wait4value;
signal old_button, button_activity: std_logic_vector(3 downto 0);

begin
--              CW
--  State   000 do nothing 
--  1       001 (increase) freq
--  2       010 (getting the base) 101 (getting the base plus one) 111 (actually add)
--  3       100 (decrease) freq
--  4       
--   sw(0) is the ready
--   sw(1) is left
--   sw(2) is up
--   sw(3) is right
--   sw(4) is down

    process(clk)
    begin
        if(rising_edge(clk)) then
            if (reset_n = '0') then
                button_activity <= "0000";
                old_button <= "0000";
            else
                button_activity <= (btn xor old_button) and btn;
                old_button <= btn;
                
           end if;
         end if;
     end process;
     
    process(clk)
    begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                state <= reset;
            else
                case state is
                    when wait4value =>
                        cw <= "000";
                        if (button_activity(3)= '1')then state <= Inc_Freq;
                        elsif (button_activity(1) = '1') then state <= Dec_freq;
                        elsif (ready = '1') then state <= Get_Base;  cw <= "111"; end if;
                    when Inc_freq =>
                        cw <= "001";
                        state <= wait4value; 
                    when Dec_freq =>
                        cw <= "100";
                        state <= wait4value;
                    when Get_Base =>
                        cw <= "010"; --reset the counter
                        state <= Get_Base_Plus;
                    when Math => 
                        state <= wait4value;
                    when Get_Base_Plus => 
                        cw <= "101";
                        state <= wait4value;
--                    when Increment_phase =>
--                        state <= Get_Base_Plus;
                    when reset =>
                        state <= wait4value;
                    
                    end case;
                    end if;
                    end if;
                    end process; 
--cw <= "110" when state = Math;
end Behavioral;
