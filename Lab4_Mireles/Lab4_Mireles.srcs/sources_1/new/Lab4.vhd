----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/03/2017 07:45:47 AM
-- Design Name: 
-- Module Name: Lab4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description:  
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Lab4 is
Port(
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC;	
	btn: in	STD_LOGIC_VECTOR(3 downto 0);
	switch: in std_logic_vector( 7 downto 0));
end Lab4;

architecture Behavioral of Lab4 is

signal sw_sig: std_logic_vector( 4 downto 0);
signal cw_sig: std_logic_vector( 2 downto 0);
signal ready: std_logic;


component Lab4_datapath is
    Port(
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC;	
	sw: out std_logic_vector(4 downto 0);
	cw: in std_logic_vector (2 downto 0);
	
	ready: out std_logic;
	switch: in std_logic_vector( 7 downto 0));

end component;

component Lab4_fsm is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           ready: in std_logic;
           btn: in	STD_LOGIC_VECTOR(3 downto 0);
		   sw: in std_logic_vector(4 downto 0);
		   cw: out std_logic_vector (2 downto 0) );
end component;
begin
    inst_Lab4_CU:  Lab4_fsm
    Port map( clk => clk,
           reset_n => reset_n,
           ready => ready,
           btn => btn,
		   sw => sw_sig,
		   cw => cw_sig );
		   
    inst_Lab4_dp: Lab4_datapath
    
    port map(clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata=> ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        scl => scl,
        sda => sda,   
        sw=> sw_sig,
        cw=> cw_sig,
        ready => ready,
        switch=> switch);




end Behavioral;
