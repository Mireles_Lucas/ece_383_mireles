----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	lab4_datapath.vhd
-- HW:		-
-- Pupr:	Implentation of datapath for lab 4
--
-- Documentation:	C2C Hayden asssited me with exporting my LUT from excel to concatinate it into notepad
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library UNIMACRO;		-- This contains links to the Xilinx block RAM
use UNIMACRO.vcomponents.all;
--use work.lab2Parts.all;	

entity Lab4_datapath is
    Port(
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC;	
	sw: out std_logic_vector(4 downto 0);
	cw: in std_logic_vector (2 downto 0);
	ready: out std_logic;
	switch: in std_logic_vector( 7 downto 0));

end Lab4_datapath;



architecture Behavioral of Lab4_datapath is

signal ready_sig, reset: std_logic;
signal D_out_Lsig, DI_sig: std_logic_vector(15 downto 0);
signal Lin_sig, R_Bus_in, L_Bus_out, R_Bus_out: std_logic_vector(17 downto 0);
signal index: unsigned( 15 downto 0) := "0000000000000000";
signal base, base_plus: unsigned( 15 downto 0);
signal phase_increment: unsigned( 7 downto 0);
signal multiplier: unsigned( 5 downto 0);
signal Read_sig: unsigned( 9 downto 0) := "0000000000";
signal BRAMin : std_logic_vector( 9 downto 0);
signal delta: unsigned ( 15 downto 0);
signal offset: unsigned (21 downto 0);
signal D_out_L: unsigned( 15 downto 0);



component Audio_Codec_Wrapper 
            Port ( clk : in STD_LOGIC;
                reset_n : in STD_LOGIC;
                ac_mclk : out STD_LOGIC;
                ac_adc_sdata : in STD_LOGIC;
                ac_dac_sdata : out STD_LOGIC;
                ac_bclk : out STD_LOGIC;
                ac_lrclk : out STD_LOGIC;
                ready : out STD_LOGIC;
                L_bus_in : in std_logic_vector(17 downto 0); -- left channel input to DAC
                R_bus_in : in std_logic_vector(17 downto 0); -- right channel input to DAC
                L_bus_out : out  std_logic_vector(17 downto 0); -- left channel output from ADC
                R_bus_out : out  std_logic_vector(17 downto 0); -- right channel output from ADC
                scl : inout STD_LOGIC;
                sda : inout STD_LOGIC);
        end component;
        
 
begin

process (clk)
    begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then -- output the value because its waiting for the ready
                    Read_sig <= (index(15 downto 6)); 
--                    phase_increment <= "10010110";
                    multiplier <= "000001";
            elsif (cw = "111") then   
                    index <= index + ("000000"&phase_increment&"00");            
                    Read_sig <= (Read_sig) + 1;       
            elsif ( cw = "010") then                     
                    multiplier <= index(5 downto 0); 
            elsif ( cw = "101") then
                    Read_sig <= (index(15 downto 6));
            end if;
            end if;
            end process;

process (clk)
    begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                    base <= "0000000000000000";
                    base_plus <= "0000000000000000";
            elsif ( cw = "010") then 
                    base <= unsigned(D_out_Lsig); -- put in parallel process
            elsif ( cw = "101") then
                    base_plus <= unsigned(D_out_Lsig); -- put in parrallel process
            end if;
            end if;
            end process;
            
process (clk)
    begin 
        if (rising_edge(clk)) then
            if (reset_n = '0') then
                phase_increment <= "10010110";
            elsif (cw = "001") then
                phase_increment <= phase_increment + ("00"&unsigned(switch(7 downto 3)));
            elsif (cw = "100") then
                phase_increment <= phase_increment - ("00"&unsigned(switch(7 downto 3)));               
            end if;
            end if;
            end process;
            
delta <= (base - base_plus) when (base_plus < base) else
         (base_plus - base);
         
offset <= multiplier * delta;

--D_out_L <= ((base + offset(21 downto 6))-x"8000") ;

D_out_L <= ((base + offset(21 downto 6))) ;

Lin_sig <= std_logic_vector( (base + offset( 21 downto 6)) - x"8000")&"00" when cw = "010" ;
--else "000000000000000000" when reset_n = '0';
--BRAMin <= std_logic_vector(Read_sig + 1) when cw = "111" else std_logic_vector(Read_sig); 
ready <= ready_sig;




reset <= not reset_n;

    inst_audio_codec_wrapper : Audio_Codec_Wrapper port map(
        clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        ready => ready_sig,
        L_bus_in => Lin_sig, -- left channel input to DAC
        R_bus_in => Lin_sig, -- right channel input to DAC
        L_bus_out => L_Bus_out, -- left channel output from ADC 
        R_bus_out => R_Bus_out, -- right channel output from ADC
        scl => scl,
        sda => sda);
        
    BRAM_L: BRAM_SDP_MACRO
            generic map (
                    BRAM_SIZE => "18Kb",			-- Target BRAM, "18Kb" or "36Kb"
                    DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6", "7SERIES"
                    DO_REG => 0,                 -- Optional output register disabled
                    INIT => X"000000000000000000",    -- Initial values on output port
                    INIT_FILE => "NONE",            -- 
                    WRITE_WIDTH => 16,            -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                    READ_WIDTH => 16,            -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                    SIM_COLLISION_CHECK => "NONE",     -- Simulation collision check
                    SRVAL => X"000000000000000000",    -- Set/Reset value for port output

                    INIT_00  => x"8BC48AFB8A33896A88A287D987118648857F84B683ED8324825B819280C98000",
                    INIT_01  => x"9833976E96A895E2951C9455938F92C89201913A90728FAB8EE38E1C8D548C8C",
                    INIT_02  => x"A467A3A6A2E5A223A161A09F9FDD9F1A9E569D939CCF9C0B9B479A8299BE98F8",
                    INIT_03  => x"B041AF87AECCAE11AD55AC99ABDCAB1FAA61A9A3A8E5A826A767A6A8A5E8A528",
                    INIT_04  => x"BBA5BAF2BA40B98CB8D8B824B76FB6BAB604B54DB496B3DEB326B26EB1B5B0FB",
                    INIT_05  => x"C675C5CDC524C47AC3D0C325C27AC1CDC121C073BFC5BF17BE67BDB8BD07BC56",
                    INIT_06  => x"D097CFFBCF5DCEBFCE20CD81CCE0CC3FCB9DCAFBCA57C9B3C90FC869C7C3C71C",
                    INIT_07  => x"D9F3D963D8D3D842D7B0D71DD689D5F5D55FD4C9D432D39AD302D268D1CED133",
                    INIT_08  => x"E271E1F0E16EE0EBE067DFE3DF5DDED7DE4FDDC7DD3DDCB3DC28DB9CDB0FDA82",
                    INIT_09  => x"E9FCE98BE919E8A6E831E7BCE746E6CEE656E5DDE563E4E8E46BE3EEE370E2F1",
                    INIT_0a  => x"F082F022EFC0EF5EEEFAEE96EE30EDC9ED61ECF8EC8EEC23EBB7EB4AEADCEA6D",
                    INIT_0b  => x"F5F3F5A4F555F504F4B2F45EF40AF3B5F35EF307F2AEF254F1F9F19DF140F0E2",
                    INIT_0c  => x"FA41FA05F9C7F989F949F908F8C6F883F83FF7FAF7B3F76BF722F6D8F68DF641",
                    INIT_0d  => x"FD61FD38FD0EFCE3FCB6FC88FC59FC29FBF7FBC5FB91FB5CFB26FAEEFAB6FA7C",
                    INIT_0e  => x"FF4DFF37FF20FF08FEEFFED5FEB9FE9CFE7EFE5FFE3EFE1CFDF9FDD5FDB0FD89",
                    INIT_0f  => x"FFFEFFFCFFF9FFF5FFEFFFE9FFE1FFD7FFCDFFC1FFB4FFA6FF97FF86FF74FF61",
                    INIT_10  => x"FF74FF86FF97FFA6FFB4FFC1FFCDFFD7FFE1FFE9FFEFFFF5FFF9FFFCFFFEFFFF",
                    INIT_11  => x"FDB0FDD5FDF9FE1CFE3EFE5FFE7EFE9CFEB9FED5FEEFFF08FF20FF37FF4DFF61",
                    INIT_12  => x"FAB6FAEEFB26FB5CFB91FBC5FBF7FC29FC59FC88FCB6FCE3FD0EFD38FD61FD89",
                    INIT_13  => x"F68DF6D8F722F76BF7B3F7FAF83FF883F8C6F908F949F989F9C7FA05FA41FA7C",
                    INIT_14  => x"F140F19DF1F9F254F2AEF307F35EF3B5F40AF45EF4B2F504F555F5A4F5F3F641",
                    INIT_15  => x"EADCEB4AEBB7EC23EC8EECF8ED61EDC9EE30EE96EEFAEF5EEFC0F022F082F0E2",
                    INIT_16  => x"E370E3EEE46BE4E8E563E5DDE656E6CEE746E7BCE831E8A6E919E98BE9FCEA6D",
                    INIT_17  => x"DB0FDB9CDC28DCB3DD3DDDC7DE4FDED7DF5DDFE3E067E0EBE16EE1F0E271E2F1",
                    INIT_18  => x"D1CED268D302D39AD432D4C9D55FD5F5D689D71DD7B0D842D8D3D963D9F3DA82",
                    INIT_19  => x"C7C3C869C90FC9B3CA57CAFBCB9DCC3FCCE0CD81CE20CEBFCF5DCFFBD097D133",
                    INIT_1a  => x"BD07BDB8BE67BF17BFC5C073C121C1CDC27AC325C3D0C47AC524C5CDC675C71C",
                    INIT_1b  => x"B1B5B26EB326B3DEB496B54DB604B6BAB76FB824B8D8B98CBA40BAF2BBA5BC56",
                    INIT_1c  => x"A5E8A6A8A767A826A8E5A9A3AA61AB1FABDCAC99AD55AE11AECCAF87B041B0FB",
                    INIT_1d  => x"99BE9A829B479C0B9CCF9D939E569F1A9FDDA09FA161A223A2E5A3A6A467A528",
                    INIT_1e  => x"8D548E1C8EE38FAB9072913A920192C8938F9455951C95E296A8976E983398F8",
                    INIT_1f  => x"80C98192825B832483ED84B6857F8648871187D988A2896A8A338AFB8BC48C8C",
                    INIT_20  => x"743C750575CD7696775E782778EF79B87A817B4A7C137CDC7DA57E6E7F378000",
                    INIT_21  => x"67CD689269586A1E6AE46BAB6C716D386DFF6EC66F8E7055711D71E472AC7374",
                    INIT_22  => x"5B995C5A5D1B5DDD5E9F5F61602360E661AA626D633163F564B9657E66426708",
                    INIT_23  => x"4FBF5079513451EF52AB5367542454E1559F565D571B57DA589959585A185AD8",
                    INIT_24  => x"445B450E45C04674472847DC4891494649FC4AB34B6A4C224CDA4D924E4B4F05",
                    INIT_25  => x"398B3A333ADC3B863C303CDB3D863E333EDF3F8D403B40E94199424842F943AA",
                    INIT_26  => x"2F69300530A3314131E0327F332033C13463350535A9364D36F13797383D38E4",
                    INIT_27  => x"260D269D272D27BE285028E329772A0B2AA12B372BCE2C662CFE2D982E322ECD",
                    INIT_28  => x"1D8F1E101E921F151F99201D20A3212921B1223922C3234D23D8246424F1257E",
                    INIT_29  => x"1604167516E7175A17CF184418BA193219AA1A231A9D1B181B951C121C901D0F",
                    INIT_2a  => x"0F7E0FDE104010A21106116A11D01237129F1308137213DD144914B615241593",
                    INIT_2b  => x"0A0D0A5C0AAB0AFC0B4E0BA20BF60C4B0CA20CF90D520DAC0E070E630EC00F1E",
                    INIT_2c  => x"05BF05FB0639067706B706F8073A077D07C10806084D089508DE0928097309BF",
                    INIT_2d  => x"029F02C802F2031D034A037803A703D70409043B046F04A404DA0512054A0584",
                    INIT_2e  => x"00B300C900E000F80111012B01470164018201A101C201E40207022B02500277",
                    INIT_2f  => x"000200040007000B00110017001F00290033003F004C005A0069007A008C009F",
                    INIT_30  => x"008C007A0069005A004C003F00330029001F00170011000B0007000400020001",
                    INIT_31  => x"0250022B020701E401C201A1018201640147012B011100F800E000C900B3009F",
                    INIT_32  => x"054A051204DA04A4046F043B040903D703A70378034A031D02F202C8029F0277",
                    INIT_33  => x"0973092808DE0895084D080607C1077D073A06F806B70677063905FB05BF0584",
                    INIT_34  => x"0EC00E630E070DAC0D520CF90CA20C4B0BF60BA20B4E0AFC0AAB0A5C0A0D09BF",
                    INIT_35  => x"152414B6144913DD13721308129F123711D0116A110610A210400FDE0F7E0F1E",
                    INIT_36  => x"1C901C121B951B181A9D1A2319AA193218BA184417CF175A16E7167516041593",
                    INIT_37  => x"24F1246423D8234D22C3223921B1212920A3201D1F991F151E921E101D8F1D0F",
                    INIT_38  => x"2E322D982CFE2C662BCE2B372AA12A0B297728E3285027BE272D269D260D257E",
                    INIT_39  => x"383D379736F1364D35A93505346333C13320327F31E0314130A330052F692ECD",
                    INIT_3a  => x"42F94248419940E9403B3F8D3EDF3E333D863CDB3C303B863ADC3A33398B38E4",
                    INIT_3b  => x"4E4B4D924CDA4C224B6A4AB349FC4946489147DC4728467445C0450E445B43AA",
                    INIT_3c  => x"5A185958589957DA571B565D559F54E15424536752AB51EF513450794FBF4F05",
                    INIT_3d  => x"6642657E64B963F56331626D61AA60E660235F615E9F5DDD5D1B5C5A5B995AD8",
                    INIT_3e  => x"72AC71E4711D70556F8E6EC66DFF6D386C716BAB6AE46A1E6958689267CD6708",
                    INIT_3f  => x"7F377E6E7DA57CDC7C137B4A7A8179B878EF7827775E769675CD7505743C7374")
            -- Set/Reset value for port output
--                
            port map (
            
                    DO => D_out_Lsig,					-- Output read data port, width defined by READ_WIDTH parameter
                    RDADDR => std_logic_vector(Read_sig),        -- Input address, width defined by port depth
                    RDCLK => clk,                 -- 1-bit input clock
                    RST => reset,                -- active high reset
                    RDEN => '1',                -- read enable 
                    REGCE => '1',                -- 1-bit input read output register enable - ignored
                    DI => DI_sig,                    -- Dummy write data - never used in this application
                    WE => "00",                -- write to neither byte
                    WRADDR => "0000000000",        -- Dummy place holder address
                    WRCLK => clk,                -- 1-bit input write clock
                    WREN => '0');                -- we are not writing to this RAM



end Behavioral;
