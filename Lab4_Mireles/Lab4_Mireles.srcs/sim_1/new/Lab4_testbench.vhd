----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/03/2017 07:59:16 PM
-- Design Name: 
-- Module Name: Lab4_testbench - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Lab4_testbench is
--  Port ( );
end Lab4_testbench;

architecture Behavioral of Lab4_testbench is

component Lab4_datapath is
    Port(
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC;	
	sw: out std_logic_vector(4 downto 0);
	cw: in std_logic_vector (2 downto 0);
--	btn: in	STD_LOGIC_VECTOR(3 downto 0);
	ready: out std_logic;
	switch: in std_logic_vector( 7 downto 0));

end component;

component Lab4_fsm is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           ready: in std_logic;
           btn: in	STD_LOGIC_VECTOR(3 downto 0);
		   sw: in std_logic_vector(4 downto 0);
		   cw: out std_logic_vector (2 downto 0) );
end component;

signal	clk : STD_LOGIC;
signal	reset_n : STD_LOGIC;
signal	ac_mclk : STD_LOGIC;
signal	ac_adc_sdata : STD_LOGIC;
signal	ac_dac_sdata :  STD_LOGIC;
signal	ac_bclk :  STD_LOGIC;
signal	ac_lrclk : STD_LOGIC;
signal	scl :  STD_LOGIC;
signal	sda :  STD_LOGIC;	
signal	sw_sig: std_logic_vector(4 downto 0);
signal	cw_sig: std_logic_vector (2 downto 0) := "000";
signal	btn: STD_LOGIC_VECTOR(3 downto 0);
signal	switch: std_logic_vector( 7 downto 0);
signal  ready_sig: std_logic;
--signal  btn: STD_LOGIC_VECTOR(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;  -- Sets clock to ~ 100MHz
--   constant BIT_CLK_period : time := 80 ns;  -- Sets Bit Clock for AC'97 to the necessary 12.288 MHz

   
begin

    UUT:  Lab4_fsm
    Port map( clk => clk,
           reset_n => reset_n,
           ready=> ready_sig,
           btn => btn,
		   sw => sw_sig,
		   cw => cw_sig );
		   
    UUT_dp: Lab4_datapath port map(clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata=> ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        scl => scl,
        sda => sda,   
        sw=> sw_sig,
        cw=> cw_sig,
--        btn=> btn,
        ready=> OPEN,
        switch=> switch); 

   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
    ready :process
   begin
       ready_sig <= '0';
        wait for 20.833 us;
        ready_sig <= '1';
        wait for clk_period;
   end process;
     reset_n <= '0', '1' after clk_period;
--     ready_sig <= '0', '1' after 10 * clk_period, '0' after 12 * clk_period, '1' after 18 * clk_period, '0' after 20 * clk_period, '1' after 26 * clk_period, '0' after 28 * clk_period, '1' after 34 * clk_period, '0' after 36 * clk_period, '1' after 42* clk_period, '0' after 44*clk_period, '1' after 50 * clk_period, '0' after 52 * clk_period, '1' after 58 * clk_period, '0' after 60 * clk_period, '1' after 66 * clk_period, '0' after 68 * clk_period, '1' after 74 * clk_period, '0' after 76 * clk_period, '1' after 82* clk_period, '0' after 84*clk_period;
end Behavioral;
