# Lab 4- Function Generator  

### By C2C Lucas Mireles

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Hardware schematic](#hardware-schematic)
4. [Debugging](#debugging)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Documentation](#documentation)
 
### Lab 4 Objectives and Purpose 
You are to use Direct Digital Synthesis to reproduce your audio waveform. You may choose any waveform so long as its not Piecewise Linear. A few interesting examples would be sinusoids, sinc, exponentially damped sinusoids, waveform from a musical instrument (guitar, piano, clarinet). Its your responsibility to get the samples for this waveform. I would suggest either deriving the waveform using a program like Python, a spreadsheet, or digitizing the samples using your Lab3. Once you have your data, it should be hardwired into the BRAM using the "init" statements. Please consult the handout associated with lesson 24 for more details on how to accomplish this.

### Preliminary Design 

The first step in the lab was to create a Block Diagram that met all the requirments for the lab. Below can be found the requirements along with the block diagram located in Milestone 1.

######Requirements
- Use an update rate of 48kHz
- At 440Hz, the LUT should be incremented by about 1 index.
- Be able to make between a 1Hz and 0.25Hz change in frequency.
- Be able to generate a full amplitude waveform.

##### Milestone 1
At the end of the first lab session, you should have a completed hardware diagram drawn in paint or another image editing software and be able to be readiable when printed onto an 8.5x11 sheet of paper. Provide me with a hardcopy of your schematic at the end of class. This diagram must contain the following.

- A border defining the top-level entity. Borders for each of the components instantiated with-in the top-level entity.
- All components must be named in the upper left corner.
- All signals entering and exiting components must have their port name defined just inside the border.
- All signals outside the components must have their width defined as well as be labeled with their names.

![](images/block.JPG)
###### Figure 1. Block Diagram of Lab 4 with all entities and process statements

The next step in the design of the lab was to actually create the hardware via VHDL.  The first step was to actually create the look up table used by the BRAM which contained all of the values needed to create the frequency.  I used Microsoft Excel to create the values along with notepad ++ to format them. Below can be found the lookup table.

    INIT_00  => x"8BC48AFB8A33896A88A287D987118648857F84B683ED8324825B819280C98000",
    INIT_01  => x"9833976E96A895E2951C9455938F92C89201913A90728FAB8EE38E1C8D548C8C",
    INIT_02  => x"A467A3A6A2E5A223A161A09F9FDD9F1A9E569D939CCF9C0B9B479A8299BE98F8",
    INIT_03  => x"B041AF87AECCAE11AD55AC99ABDCAB1FAA61A9A3A8E5A826A767A6A8A5E8A528",
    INIT_04  => x"BBA5BAF2BA40B98CB8D8B824B76FB6BAB604B54DB496B3DEB326B26EB1B5B0FB",
    INIT_05  => x"C675C5CDC524C47AC3D0C325C27AC1CDC121C073BFC5BF17BE67BDB8BD07BC56",
    INIT_06  => x"D097CFFBCF5DCEBFCE20CD81CCE0CC3FCB9DCAFBCA57C9B3C90FC869C7C3C71C",
    INIT_07  => x"D9F3D963D8D3D842D7B0D71DD689D5F5D55FD4C9D432D39AD302D268D1CED133",
    INIT_08  => x"E271E1F0E16EE0EBE067DFE3DF5DDED7DE4FDDC7DD3DDCB3DC28DB9CDB0FDA82",
    INIT_09  => x"E9FCE98BE919E8A6E831E7BCE746E6CEE656E5DDE563E4E8E46BE3EEE370E2F1",
    INIT_0a  => x"F082F022EFC0EF5EEEFAEE96EE30EDC9ED61ECF8EC8EEC23EBB7EB4AEADCEA6D",
    INIT_0b  => x"F5F3F5A4F555F504F4B2F45EF40AF3B5F35EF307F2AEF254F1F9F19DF140F0E2",
    INIT_0c  => x"FA41FA05F9C7F989F949F908F8C6F883F83FF7FAF7B3F76BF722F6D8F68DF641",
    INIT_0d  => x"FD61FD38FD0EFCE3FCB6FC88FC59FC29FBF7FBC5FB91FB5CFB26FAEEFAB6FA7C",
    INIT_0e  => x"FF4DFF37FF20FF08FEEFFED5FEB9FE9CFE7EFE5FFE3EFE1CFDF9FDD5FDB0FD89",
    INIT_0f  => x"FFFEFFFCFFF9FFF5FFEFFFE9FFE1FFD7FFCDFFC1FFB4FFA6FF97FF86FF74FF61",
    INIT_10  => x"FF74FF86FF97FFA6FFB4FFC1FFCDFFD7FFE1FFE9FFEFFFF5FFF9FFFCFFFEFFFF",
    INIT_11  => x"FDB0FDD5FDF9FE1CFE3EFE5FFE7EFE9CFEB9FED5FEEFFF08FF20FF37FF4DFF61",
    INIT_12  => x"FAB6FAEEFB26FB5CFB91FBC5FBF7FC29FC59FC88FCB6FCE3FD0EFD38FD61FD89",
    INIT_13  => x"F68DF6D8F722F76BF7B3F7FAF83FF883F8C6F908F949F989F9C7FA05FA41FA7C",
    INIT_14  => x"F140F19DF1F9F254F2AEF307F35EF3B5F40AF45EF4B2F504F555F5A4F5F3F641",
    INIT_15  => x"EADCEB4AEBB7EC23EC8EECF8ED61EDC9EE30EE96EEFAEF5EEFC0F022F082F0E2",
    INIT_16  => x"E370E3EEE46BE4E8E563E5DDE656E6CEE746E7BCE831E8A6E919E98BE9FCEA6D",
    INIT_17  => x"DB0FDB9CDC28DCB3DD3DDDC7DE4FDED7DF5DDFE3E067E0EBE16EE1F0E271E2F1",
    INIT_18  => x"D1CED268D302D39AD432D4C9D55FD5F5D689D71DD7B0D842D8D3D963D9F3DA82",
    INIT_19  => x"C7C3C869C90FC9B3CA57CAFBCB9DCC3FCCE0CD81CE20CEBFCF5DCFFBD097D133",
    INIT_1a  => x"BD07BDB8BE67BF17BFC5C073C121C1CDC27AC325C3D0C47AC524C5CDC675C71C",
    INIT_1b  => x"B1B5B26EB326B3DEB496B54DB604B6BAB76FB824B8D8B98CBA40BAF2BBA5BC56",
    INIT_1c  => x"A5E8A6A8A767A826A8E5A9A3AA61AB1FABDCAC99AD55AE11AECCAF87B041B0FB",
    INIT_1d  => x"99BE9A829B479C0B9CCF9D939E569F1A9FDDA09FA161A223A2E5A3A6A467A528",
    INIT_1e  => x"8D548E1C8EE38FAB9072913A920192C8938F9455951C95E296A8976E983398F8",
    INIT_1f  => x"80C98192825B832483ED84B6857F8648871187D988A2896A8A338AFB8BC48C8C",
    INIT_20  => x"743C750575CD7696775E782778EF79B87A817B4A7C137CDC7DA57E6E7F378000",
    INIT_21  => x"67CD689269586A1E6AE46BAB6C716D386DFF6EC66F8E7055711D71E472AC7374",
    INIT_22  => x"5B995C5A5D1B5DDD5E9F5F61602360E661AA626D633163F564B9657E66426708",
    INIT_23  => x"4FBF5079513451EF52AB5367542454E1559F565D571B57DA589959585A185AD8",
    INIT_24  => x"445B450E45C04674472847DC4891494649FC4AB34B6A4C224CDA4D924E4B4F05",
    INIT_25  => x"398B3A333ADC3B863C303CDB3D863E333EDF3F8D403B40E94199424842F943AA",
    INIT_26  => x"2F69300530A3314131E0327F332033C13463350535A9364D36F13797383D38E4",
    INIT_27  => x"260D269D272D27BE285028E329772A0B2AA12B372BCE2C662CFE2D982E322ECD",
    INIT_28  => x"1D8F1E101E921F151F99201D20A3212921B1223922C3234D23D8246424F1257E",
    INIT_29  => x"1604167516E7175A17CF184418BA193219AA1A231A9D1B181B951C121C901D0F",
    INIT_2a  => x"0F7E0FDE104010A21106116A11D01237129F1308137213DD144914B615241593",
    INIT_2b  => x"0A0D0A5C0AAB0AFC0B4E0BA20BF60C4B0CA20CF90D520DAC0E070E630EC00F1E",
    INIT_2c  => x"05BF05FB0639067706B706F8073A077D07C10806084D089508DE0928097309BF",
    INIT_2d  => x"029F02C802F2031D034A037803A703D70409043B046F04A404DA0512054A0584",
    INIT_2e  => x"00B300C900E000F80111012B01470164018201A101C201E40207022B02500277",
    INIT_2f  => x"000200040007000B00110017001F00290033003F004C005A0069007A008C009F",
    INIT_30  => x"008C007A0069005A004C003F00330029001F00170011000B0007000400020001",
    INIT_31  => x"0250022B020701E401C201A1018201640147012B011100F800E000C900B3009F",
    INIT_32  => x"054A051204DA04A4046F043B040903D703A70378034A031D02F202C8029F0277",
    INIT_33  => x"0973092808DE0895084D080607C1077D073A06F806B70677063905FB05BF0584",
    INIT_34  => x"0EC00E630E070DAC0D520CF90CA20C4B0BF60BA20B4E0AFC0AAB0A5C0A0D09BF",
    INIT_35  => x"152414B6144913DD13721308129F123711D0116A110610A210400FDE0F7E0F1E",
    INIT_36  => x"1C901C121B951B181A9D1A2319AA193218BA184417CF175A16E7167516041593",
    INIT_37  => x"24F1246423D8234D22C3223921B1212920A3201D1F991F151E921E101D8F1D0F",
    INIT_38  => x"2E322D982CFE2C662BCE2B372AA12A0B297728E3285027BE272D269D260D257E",
    INIT_39  => x"383D379736F1364D35A93505346333C13320327F31E0314130A330052F692ECD",
    INIT_3a  => x"42F94248419940E9403B3F8D3EDF3E333D863CDB3C303B863ADC3A33398B38E4",
    INIT_3b  => x"4E4B4D924CDA4C224B6A4AB349FC4946489147DC4728467445C0450E445B43AA",
    INIT_3c  => x"5A185958589957DA571B565D559F54E15424536752AB51EF513450794FBF4F05",
    INIT_3d  => x"6642657E64B963F56331626D61AA60E660235F615E9F5DDD5D1B5C5A5B995AD8",
    INIT_3e  => x"72AC71E4711D70556F8E6EC66DFF6D386C716BAB6AE46A1E6958689267CD6708",
    INIT_3f  => x"7F377E6E7DA57CDC7C137B4A7A8179B878EF7827775E769675CD7505743C7374"

Once the LUT was complete, it was time to create both my FSM and my datapath for my lab.  The states of my FSM can be found on block diagram. In addition to my FSM, the datapath and all of its process were then completed to handle interpolation based off of my phase increment which is controlled using my switches.  Control words (cw) were the primary source of controlling my read addresses inside of the process. The control words outputted by the FSM are as follows:

    CW  	 Response

    000 	do nothing 
    001 	increase freq
    010 	getting the base
    101 	getting the base plus one
    111 	actually add
    100 	decrease freq

Once all of the hardware was complete, it was time to create a testbench in order to see if my hardware was working correctly. In Milestone 2, a more in depth analysis is shown about my test bench.

##### Milestone 2
At the end of the second lab period, you should have a working testbench. When simulating your design, have the testbench supply a mock ready signal in place of the ready signal generated the Audio_Codec_Wrapper (when put in a testbench, the Audio_Codec_Wrapper is not able to generate a ready signal without a lot of extra work). 

When complete, I expect your timing diagram to look like the image below and contain at least:
clk
reset
ready (simulated using CSA statements in teshbench)
FSM state
BRAM address
Phase increment
BRAM data out
Amplitude cofficient (if aiming for B or A functionality)
Multiplied data out (if aiming for B or A functionality)
Slide switches
Button values
Your simulation needs to simulate a button press (and release) to change the phase increment. After that is done, you need to show that the BRAM address is being incremented by your new phase increment value. 

![](images/MS2.JPG)
##### Test Bench for Lab 4
### Debugging
 The first issue that I encountered with my Lab was the fact that my look up table was not recording the correct values for my sine wave. My sine wave was not quite reaching FFFF at its peak therefore there was almost no audio coming out because the amplitude was so low. The next biggest issue that I was having was that I was not actually driving the ready signal from the audio codec wrapper out from my datapath because I was using a dummy signal in my testbench.
 
### Testing methodology or results

#######Required Functionality
- Use the slide switches and push buttons to manipulate the phase angle and the amplitude of the waveform as follows:
- Pressing the left button should decrease the frequency of the waveform by the amount set on the slide switches.
- Pressing the right button should increase the frequency of the waveform by the amount set on the slide switches.
- The waveform should be played back through the Audio Codec interface. Remember to wait for the ready signal.
#######B-level Functionality
- Pressing the up button should increase the amplitude of the waveform by the amount set on the slide switches.
- Pressing the down button should decrease the amplitude of the waveform by the amount set on the slide switches.
- Pressing the center button should toggle between 2-different waveforms.
#######A-level Functionality
- Use the microBlaze to capture a keyboard input to manipulate the amplitude and frequency. The user will enter in an integer frequency and you are to produce a waveform with that frequency.
#######Bonus Functionality
- Connect your A-level Function Generator hardware to your Lab 3 functionality to enable the display of your generated signals on your Lab 3 O'scope display.


[Proof of Functionality](https://youtu.be/pAXnZVSzkzU)
########## Video of functionality 
![](images/functionality.jpg)

############ Waveform of interpolation
![](images/bonus.jpg)

############ Proof of Bonus

 
### Observations and Conclusions
 The objective of this lab was to use to my FPGA to store a LUT in a BRAM and access it via hardware that I designed to output a frequency.  I needed to use the buttons to increase and decrease the frequency outputted along with the switches to determine by how much I need to increase or decrease them by. Based off of this, I would say that I have successfully completed this lab.
 
 I will use the experience gained from this lab in future projects like our final project in order to successfully communicate audio from my FPGA.

### Documentation
 None.