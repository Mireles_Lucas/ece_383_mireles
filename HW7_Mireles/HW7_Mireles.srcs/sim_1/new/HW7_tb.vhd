----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	HW7_tb.vhd
-- HW:		HW7
-- Pupr:	test bench for HW 7
--
-- Documentation:	C2C hanson assisted me with getting the timing right for 
-- for the buttons to appear they are supposed to
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity HW7_tb is
--  Port ( );
end HW7_tb;

architecture Behavioral of HW7_tb is

component Top is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           button : in STD_LOGIC_VECTOR (1 downto 0);
           count : out unsigned (3 downto 0));
end component;

signal clk: STD_LOGIC := '0';
signal reset: STD_LOGIC := '0';
signal left: STD_LOGIC := '0';
signal right: STD_LOGIC := '0';
signal count: unsigned (3 downto 0) := "0000";

constant clk_period : time := 10 ns;

begin

    uut: Top port map (
    clk => clk,
    reset => reset,
    button(0) => right,
    button(1) => left,
    count => count );
    
    clk_process :process
         begin
              clk <= '0';
              wait for clk_period/2;
              clk <= '1';
              wait for clk_period/2;
     end process;
     
     
     reset <= '1' after clk_period;
     left <= '1' after clk_period*2, '0' after clk_period*4, '1' after clk_period*6, '0' after clk_period*8, '1' after clk_period*20, '0' after clk_period*22;
     right <= '1' after clk_period*10, '0' after clk_period*12, '1' after clk_period*14, '0' after clk_period*16, '1' after clk_period*24, '0' after clk_period*26; 

end Behavioral;
