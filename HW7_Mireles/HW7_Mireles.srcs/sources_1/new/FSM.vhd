----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	FSM.vhd
-- HW:		HW7
-- Pupr:	has the FSM and all states
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity FSM is
	Port(	clk: in  STD_LOGIC;
			reset : in  STD_LOGIC;
			sw: in STD_LOGIC_VECTOR(1 downto 0);
			cnt_ctl: out STd_LOGIC_VECTOR(1 downto 0) );
end FSM;




architecture behavior of FSM is

	type state_type is (WaitRight, Left, Right, Count, Init);
	signal state: state_type;
	signal cw: STD_LOGIC_VECTOR(1 downto 0);
	
		-- helps keep status bits straight
	constant left_btn: integer := 1;
	constant right_btn: integer := 0;
	
begin
	
	------------------------------------------------------------------------------
	-- 		MEMORY INPUT EQUATIONS
	-- 
	--     sw(1) left button
	--     sw(0) right button
	------------------------------------------------------------------------------	
    state_process: process(clk)
	 begin
		if (rising_edge(clk)) then
			if (reset = '0') then 
				state <= Init;
			else
				case state is
					when Init =>
					     cnt_ctl <= "00";
						 if (sw(left_btn) = '1') then state <= Left; end if;
					when Left =>
                         if (sw(left_btn) = '0') then state <= WaitRight; end if;
					when WaitRight =>
                         if (sw(right_btn) = '1') then state <= Right; end if;
					when Right =>
						 if (sw(right_btn) = '0') then state <= Count; cnt_ctl <= "01"; end if;
				    when Count =>
				         cnt_ctl <= "00";
				         state <= Init;
				end case;
			end if;
		end if;
	end process;


	------------------------------------------------------------------------------
   -- cw <= '01' when we want to count 
   --       '00' want to hold
   --       
	------------------------------------------------------------------------------	
end behavior;	