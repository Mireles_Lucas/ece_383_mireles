----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	Top.vhd
-- HW:		HW7
-- Pupr:	Top for HW 7
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Top is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           button : in STD_LOGIC_VECTOR (1 downto 0);
           count : out unsigned (3 downto 0));
end Top;

architecture Behavioral of Top is

component FSM is 
Port(	    clk: in  STD_LOGIC;
			reset : in  STD_LOGIC;
			sw: in STD_LOGIC_VECTOR(1 downto 0);
			cnt_ctl: out STD_LOGIC_VECTOR (1 downto 0) );
end component;		
component HW7 is
	generic (N: integer := 4);
    Port(   clk: in  STD_LOGIC;
            reset : in  STD_LOGIC;
            ctrl: in std_logic_vector(1 downto 0);
            D: in unsigned (N-1 downto 0);
            Q: out unsigned (N-1 downto 0));
end component;

signal cnt_ctrl : STD_LOGIC_VECTOR(1 downto 0) := "00";
signal count_sig : unsigned(3 downto 0) := "0000";

begin

    FSM_F : FSM
    port map ( clk => clk,
               reset => reset,
               sw(0) => button(0),
               sw(1) => button(1),
               cnt_ctl => cnt_ctrl );
               
    counter : HW7
    port map ( clk => clk,
               reset => reset,
               ctrl => cnt_ctrl,
               D => "0000",
               Q => count_sig );
    
    count <= count_sig;

end Behavioral;
