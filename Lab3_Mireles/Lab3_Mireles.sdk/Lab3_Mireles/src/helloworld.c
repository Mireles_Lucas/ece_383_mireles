/*--------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	March 7, 2016
-- File:	helloworld.c
-- Event:	Lab 3
-- Crs:		ECE 383
--
-- Purp:	Connects my oscope ip to the microblaze
--
-- Documentation:	C2C Hanson assisted me with finding the wrAddr and how to shift my bits in order to successfully print them on the screen.
					C2C Hayden assisted me with storing my values in a buffer and only counting to 640 when writing the wrAddr
--
-- Academic Integrity Statement: I certify that, while others may have
-- assisted me in brain storming, debugging and validating this program,
-- the program itself is my own work. I understand that submitting code
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to
-- another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/***************************** Include Files ********************************/

#include "xparameters.h"
#include "stdio.h"
#include "xstatus.h"

#include "platform.h"
#include "xil_printf.h"						// Contains xil_printf
#include <xuartlite_l.h>					// Contains XUartLite_RecvByte
#include <xil_io.h>							// Contains Xil_Out8 and its variations
#include <xil_exception.h>

/************************** Constant Definitions ****************************/

/*
 * The following constants define the slave registers used for Lab 3
 */
#define O_Base				0x44a00000

#define O_flagQReg			O_Base		// slv_reg0
#define	O_flagClearReg			O_Base		// slv_reg0
#define	O_exWrAddrReg			O_Base + 4	// slv_reg1
#define	O_exWenReg			O_Base + 8  // slv_reg2
#define	O_exSelReg				O_Base + 12	// slv_reg3
#define	O_L_bus_outReg			O_Base + 16	// slv_reg4
#define	O_R_bus_outReg			O_Base + 20  // slv_reg5
#define	O_trigger_timeReg		O_Base + 24	// slv_reg6
#define	O_trigger_voltReg		O_Base + 28	// slv_reg7
#define	O_exLbusReg			O_Base + 32  // slv_reg8
#define	O_exRbusReg			O_Base + 36  // slv_reg9

#define	uartRegAddr			0x40600000		// read <= RX, write => TX

#define printf xil_printf			/* A smaller footprint printf */

#define ready_Bit			0x04

/************************** Function Prototypes ****************************/
void myISR(void);

/************************** Variable Definitions **************************/
/*
 * The following are declared globally so they are zeroed and so they are
 * easily accessible from a debugger
 */

u16 trigger_time = 320;  // starts the time and voltage in the middle
u16 trigger_volt = 220;

u16 L_Buff[sizeof(u16)*1024]; // creating the buffers to store the L and  R bus data in
u16 R_Buff[sizeof(u16)*1024];

u16 old_level = 0;
u16 new_level = 0;

int main(void){
		unsigned char c;

		init_platform();

		Xil_Out16(O_exWenReg, 0x00);
		Xil_Out16(O_exSelReg, 0x01);

		Xil_Out16(O_trigger_timeReg, trigger_time);
		Xil_Out16(O_trigger_voltReg, trigger_volt);

	    microblaze_register_handler((XInterruptHandler) myISR, (void *) 0);
	    microblaze_enable_interrupts();

	    Xil_Out16(O_flagClearReg, ready_Bit);					// Clear the ready bit
		Xil_Out16(O_flagClearReg, 0x00);					// allow the flag to be reset later

		print("Welcome to Lab3\n\r");
		printf("--------------------------\r\n");
		printf("	Trigger Time  = %d\r\n",Xil_In16(O_trigger_timeReg));
		printf("	Trigger Volt = %d\r\n",Xil_In16(O_trigger_voltReg));
		printf("    L Bus out = %d\r\n", Xil_In16(O_L_bus_outReg));
		printf("--------------------------\r\n");
		printf("?: help menu\r\n");
		printf("w: move trigger volt up\r\n");
		printf("s: move trigger volt down\r\n");
		printf("a: move trigger time left\r\n");
		printf("d: move trigger time right\r\n");
		printf("c: change trigger channel \r\n");

		while (1){

			if (XUartLite_IsReceiveEmpty(uartRegAddr)){}
			else{
				c=XUartLite_RecvByte(uartRegAddr);
					switch(c){
					case'?': //help menu same as the display
						printf("--------------------------\r\n");
								printf("	Trigger Time  = %d\r\n",Xil_In16(O_trigger_timeReg));
								printf("	Trigger Volt = %d\r\n",Xil_In16(O_trigger_voltReg));
								printf("    L Bus out = %d\r\n", Xil_In16(O_L_bus_outReg));

								printf("--------------------------\r\n");
								printf("?: help menu\r\n");
								printf("w: move trigger volt up\r\n");
								printf("s: move trigger volt down\r\n");
								printf("a: move trigger time left\r\n");
								printf("d: move trigger time right\r\n");
								break;
					case'w': //move up
							if (trigger_volt >= 30){
								trigger_volt -= 10;
							}
							else if (trigger_volt < 30){
								trigger_volt = 20;
							}
							Xil_Out16(O_trigger_voltReg, trigger_volt);
							break;

					case's': // move down
							if (trigger_volt <= 410){
								trigger_volt += 10;
							}
							else if (trigger_volt > 410){
								trigger_volt = 420;
							}
							Xil_Out16(O_trigger_voltReg, trigger_volt);
							break;
					case'a': //move left
							if (trigger_time >= 30){
								trigger_time -= 10;
							}
							else if (trigger_time < 30){
								trigger_time = 20;
							}
							Xil_Out16(O_trigger_timeReg, trigger_time);
							break;
					case'd'://move right
							if (trigger_time <= 610){
								trigger_time += 10;
							}
							else if (trigger_time > 610){
								trigger_time = 620;
							}
							Xil_Out16(O_trigger_timeReg, trigger_time);
							break;

					default: printf(" please enter a character from the help menu");
							 break;
							}
			}
					// need to move samples from board into memory on microblaze

					for (int i = 0; i <= 1023; i ++){
						while((Xil_In8(O_flagQReg) & ready_Bit) != ready_Bit){ //0x04 should be my ready bit because ready bit is the third bit coming out of the flagQ
									//wait until its high
								}

						L_Buff[i] = Xil_In16(O_L_bus_outReg);
						R_Buff[i] = Xil_In16(O_R_bus_outReg);

						Xil_Out8(O_flagClearReg, ready_Bit); // my flag register is only 8 bits
						Xil_Out8(O_flagClearReg, 0x00);


						}

					//need to look for a trigger event


					int trigger = 0;


					// looking for trigger?
					int i = 1;
					while (i < 1023 && trigger != 1){

					old_level = ((L_Buff[i-1])>>6) - 292;


					new_level = ((L_Buff[i])>>6) -292;

						if ((new_level < trigger_volt) && (old_level >= trigger_volt)){
							if (i >= trigger_time){
								trigger = 1;
							}
						}
						i += 1;
					}

					int diff = i - trigger_time;
					if (trigger == 1){


						for (int n = 0; n < i; n++){
							Xil_Out16(O_exLbusReg, L_Buff[i  - trigger_time + n]);
							Xil_Out16(O_exRbusReg, R_Buff[i  - trigger_time + n]);
							Xil_Out16(O_exWrAddrReg, n);
							Xil_Out16(O_exWenReg, 0x01);
							Xil_Out16(O_exWenReg, 0x00); // stop writing
						}
						while (i < 640){
							Xil_Out16(O_exLbusReg, L_Buff[i]);
							Xil_Out16(O_exRbusReg, R_Buff[i]);
							Xil_Out16(O_exWrAddrReg, i - diff);
							Xil_Out16(O_exWenReg, 0x01); // stop writing
							Xil_Out16(O_exWenReg, 0x00); // stop writing
							i += 1;
						}
						}


					}
		cleanup_platform();
		return 0;

		}


void myISR(void){
	return;
}

