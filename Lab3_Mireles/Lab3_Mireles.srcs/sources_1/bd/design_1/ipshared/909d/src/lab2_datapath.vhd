----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	lab2_datapath.vhd
-- HW:		-
-- Pupr:	Implentation of datapath for lab 2
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library UNIMACRO;		-- This contains links to the Xilinx block RAM
use UNIMACRO.vcomponents.all;
use work.lab2Parts.all;	

entity lab2_datapath is
    Port(
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC;	
	tmds : out  STD_LOGIC_VECTOR (3 downto 0);
	tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
	sw: out std_logic_vector(2 downto 0);
	cw: in std_logic_vector (2 downto 0);
--	btn: in	STD_LOGIC_VECTOR(4 downto 0);
	exWrAddr: in std_logic_vector(9 downto 0);
	exWen, exSel: in std_logic;
	Lbus_out, Rbus_out: out std_logic_vector(15 downto 0);
	exLbus, exRbus: in std_logic_vector(15 downto 0);
	flagQ: out std_logic_vector(7 downto 0);
	flagClear: in std_logic_vector(7 downto 0);
	trigger_time: in unsigned( 9 downto 0);
	trigger_volt: in unsigned( 9 downto 0));
end lab2_datapath;

architecture Behavioral of lab2_datapath is

    signal row, column: unsigned(9 downto 0);
--	signal trigger_time: unsigned (9 downto 0):= to_unsigned(200,10);
--	signal trigger_volt: unsigned (9 downto 0):= to_unsigned(220,10);
--	signal old_button, button_activity: std_logic_vector(4 downto 0);
	signal ch1_wave, ch2_wave, v_synch: std_logic := '0';
	signal ready, reset, WREN_sig, compare: std_logic := '0';
	signal L_Bus_in, R_Bus_in, L_Bus_out, R_Bus_out: STD_LOGIC_VECTOR (17 downto 0) := (others => '0');
	signal set: STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
    signal WRADDR_sig : STD_LOGIC_VECTOR(9 downto 0) := (others => '0');
    signal write_cntr : unsigned(9 downto 0) := (others => '0');
    signal sign2Unsign, sign2Unsign_sig: unsigned(17 downto 0) := (others => '0');

    
    signal DI_sig, readL : std_logic_vector (17 downto 0) := (others => '0');
    signal new_level, old_level : unsigned (9 downto 0);
    
    signal DI_sigR, readR : std_logic_vector (17 downto 0) := (others => '0');
    signal new_levelR, old_levelR : unsigned (9 downto 0);
    signal sign2UnsignR, sign2Unsign_sigR: unsigned(17 downto 0) := (others => '0');
	
    
    
begin

	------------------------------------------------------------------------------
	-- the variable button_activity will contain a '1' in any position which 
	-- has been pressed or released.  The buttons are all nominally 0
	-- and equal to 1 when pressed.
	------------------------------------------------------------------------------
--    notreallydeboncing : process(clk)
--    begin
--        if(rising_edge(clk)) then
            
--            if (reset_n = '0') then
--                button_activity <= "00000";
--                old_button <= "00000";
--            else
--                button_activity <= (btn xor old_button) and btn;
--                old_button <= btn;
                
--           end if;
--         end if;
--     end process;

	------------------------------------------------------------------------------
	-- If a button has been pressed then increment of decrement the trigger vars
	------------------------------------------------------------------------------
--	increment : process(clk)
--	begin
--	       if(rising_edge(clk)) then
--	           if (button_activity(0) = '1' and row > 20) then
--	               trigger_volt <= trigger_volt - 5;
--	           elsif (button_activity(2) = '1' and row < 420) then
--	               trigger_volt <= trigger_volt + 5;
--	           elsif (button_activity(1) = '1' and column > 20) then
--                   trigger_time <= trigger_time - 5;
--               elsif (button_activity(3) = '1' and column < 620) then
--                   trigger_time <= trigger_time + 5;
--               end if;
--             end if;
--     end process;
	           
	           
	------------------------------------------------------------------------------
	------------------------------------------------------------------------------
	loopback : process (clk)
    begin
        if (rising_edge(clk)) then

            if reset_n = '0' then
                L_bus_in <= (others => '0');
                R_bus_in <= (others => '0');                
            elsif(ready = '1') then
                L_bus_in <= L_bus_out;
                R_bus_in <= R_bus_out;

            end if;
        end if;
    end process;
    ------------------------------------------------------------------------------
    ------------------------------------------------------------------------------
	cw_counter: process (clk)
    begin
        if (rising_edge(clk)) then
            if cw(1 downto 0) = "11" then --reset
                write_cntr <= to_unsigned(23, 10);
            elsif cw(1 downto 0) = "01" and write_cntr < 1023 then --count because theres space
                write_cntr <= write_cntr + 1;
                sw(0) <= '0';
                compare <= '0';
            elsif cw(1 downto 0 ) = "01" then -- no space 
                sw(0) <= '1';    
                compare <= '1';               -- sw(0) indicates that there is no more space and the counter needs to reset
            elsif cw(1 downto 0) = "10" then
                write_cntr <= write_cntr;
            end if;
        end if;
    end process; 
    
	------------------------------------------------------------------------------
    ------------------------------------------------------------------------------
    --Might have to do more here 
    --Might have to do more here 
    --Might have to do more here 
    --Might have to do more here 
    
    sign2Unsign <= unsigned(L_Bus_in); --Might have to do more here 
    new_level <= sign2Unsign_sig(17 downto 8) - 292;
    sign2Unsign_sig <= sign2Unsign + "100000000000000000";
    
    sign2UnsignR <= unsigned(R_Bus_in); --Might have to do more here 
    new_levelR <= sign2Unsign_sigR(17 downto 8) - 292;
    sign2Unsign_sigR <= sign2UnsignR + "100000000000000000";
    --Might have to do more here 
    --Might have to do more here 
    --Might have to do more here 
	------------------------------------------------------------------------------
    ------------------------------------------------------------------------------
    compare_trigger : process (clk)
    begin
        if (rising_edge(clk)) then
        if (ready = '1') then   
            old_level <= new_level;
            old_levelR <= new_levelR;
        end if;
        end if;
    end process;
    
    -- signal words based off of comparators 
    sw(1) <= ready;  -- sw(1) is the ready signal
    sw(2) <= '1' when(trigger_volt < old_level and new_level < trigger_volt) else
             '0';
    
    ------------------------------------------------------------------------------
    ------------------------------------------------------------------------------
    reset <= not reset_n; -- do this becasue I need an active high reset for the BRAM
    
    -- writes the ch1 when readL and row are equal
    ch1_wave <= '1' when (unsigned(readL(17 downto 8))- 292 = row ) and column > 23 and column < 620 else --only care about the top 10 bits
                '0';
    ch2_wave <= '1' when (unsigned(readR(17 downto 8))- 292 = row ) and column > 23 and column < 620 else --only care about the top 10 bits
                                '0';
    
    WREN_sig <=  exWEN when exSel = '1' else
                 cw(2);
                 
    DI_sig <= (exLbus & "00") when exSel = '1' else
              std_logic_vector(sign2Unsign_sig);
    LBus_out <= std_logic_vector(sign2Unsign_sig(17 downto 2));
    DI_sigR <= (exRbus & "00") when exSel = '1' else
                            std_logic_vector(sign2Unsign_sigR);
    RBus_out <= std_logic_vector(sign2Unsign_sigR(17 downto 2));
    WRADDR_sig <= exWrAddr when exSel = '1' else
                  std_logic_vector(write_cntr);
    
	video_inst: video port map( 
		clk => clk,
		reset_n => reset_n,
		tmds => tmds,
		tmdsb => tmdsb,
		trigger_time => trigger_time,
		trigger_volt => trigger_volt,
		row => row, 
		column => column,
		ch1 => ch1_wave,
		ch1_enb => '1',
		ch2 => ch2_wave,
		ch2_enb => '1',
		v_synch => v_synch); 
		
    inst_audio_codec_wrapper : Audio_Codec_Wrapper port map(
        clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        ready => ready,
        L_bus_in => L_Bus_in, -- left channel input to DAC
        R_bus_in => R_Bus_in, -- right channel input to DAC
        L_bus_out => L_Bus_out, -- left channel output from ADC
        R_bus_out => R_Bus_out, -- right channel output from ADC
        scl => scl,
        sda => sda );
        
        sampleMemory: BRAM_SDP_MACRO
                generic map (
                    BRAM_SIZE => "18Kb",                     -- Target BRAM, "18Kb" or "36Kb"
                    DEVICE => "7SERIES",                     -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
                    DO_REG => 0,                             -- Optional output register disabled
                    INIT => X"000000000000000000",            -- Initial values on output port
                    INIT_FILE => "NONE",                    -- Not sure how to initialize the RAM from a file
                    WRITE_WIDTH => 18,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                    READ_WIDTH => 18,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                    SIM_COLLISION_CHECK => "NONE",             -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
                    SRVAL => X"000000000000000000")            -- Set/Reset value for port output
                port map (
                    DO => readL,                        -- Output read data port, width defined by READ_WIDTH parameter
                    RDADDR => STD_LOGIC_VECTOR(column),                    -- Input address, width defined by port depth
                    RDCLK => clk,                             -- 1-bit input clock
                    RST => reset,                            -- active high reset
                    RDEN => '1',                            -- read enable 
                    REGCE => '1',                            -- 1-bit input read output register enable - ignored
                    DI => DI_sig,                        -- Input data port, width defined by WRITE_WIDTH parameter
                    WE => cw(2 downto 1),                    -- since RAM is byte read, this determines high or low byte
                    WRADDR => WRADDR_sig,                    -- Input write address, width defined by write port depth
                    WRCLK => clk,                            -- 1-bit input write clock
                    WREN => WREN_sig);                             -- 1-bit input write port enable
          Channel2: BRAM_SDP_MACRO
                generic map (
                    BRAM_SIZE => "18Kb",                     -- Target BRAM, "18Kb" or "36Kb"
                    DEVICE => "7SERIES",                     -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
                    DO_REG => 0,                             -- Optional output register disabled
                    INIT => X"000000000000000000",            -- Initial values on output port
                    INIT_FILE => "NONE",                    -- Not sure how to initialize the RAM from a file
                    WRITE_WIDTH => 18,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                    READ_WIDTH => 18,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                    SIM_COLLISION_CHECK => "NONE",             -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
                    SRVAL => X"000000000000000000")            -- Set/Reset value for port output
                port map (
                    DO => readR,                        -- Output read data port, width defined by READ_WIDTH parameter
                    RDADDR => STD_LOGIC_VECTOR(column),                    -- Input address, width defined by port depth
                    RDCLK => clk,                             -- 1-bit input clock
                    RST => reset,                            -- active high reset
                    RDEN => '1',                            -- read enable 
                    REGCE => '1',                            -- 1-bit input read output register enable - ignored
                    DI => DI_sigR,                        -- Input data port, width defined by WRITE_WIDTH parameter
                    WE => cw(2 downto 1),                    -- since RAM is byte read, this determines high or low byte
                    WRADDR => WRADDR_sig,                    -- Input write address, width defined by write port depth
                    WRCLK => clk,                            -- 1-bit input write clock
                    WREN => WREN_sig);  
         
         set <= "00000"&ready&v_synch&compare;

         FR : flagRegister
                Generic Map(8)
                Port Map (clk, reset_n, set, flagClear, flagQ);
end Behavioral;
