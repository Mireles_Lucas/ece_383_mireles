----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	counter_r.vhd
-- HW:		-
-- Pupr:	row counter for lab 1
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter_r is
    Port ( clk : in STD_LOGIC;
           reset_n : in STD_LOGIC;
           ctrl : in STD_LOGIC;
           roll : out  STD_LOGIC;
           row : out unsigned (9 downto 0));
end counter_r;

architecture Behavioral of counter_r is

    signal processQ: unsigned (9 downto 0);
begin
    process(clk)
	begin
		if (rising_edge(clk)) then
			if (reset_n = '0') then
				processQ <= (others => '0');
				
			elsif ((processQ < 524) and (ctrl = '1')) then
				processQ <= processQ + 1;
                
            elsif ((ctrl = '0')) then
                processQ <= processQ;
                
			elsif ((processQ >= 524) and (ctrl = '1')) then
				processQ <= (others => '0');
				
			end if;
		end if;
	end process;
 
	-- CSA
	row <= processQ;
    roll <= '1' when (processQ = 524 and ctrl = '1') else '0';
end Behavioral;
