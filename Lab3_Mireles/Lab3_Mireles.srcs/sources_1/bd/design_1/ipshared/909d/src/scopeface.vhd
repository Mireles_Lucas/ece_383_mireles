----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	scopeface.vhd
-- HW:		-
-- Pupr:	scopeface for lab 1
--
-- Documentation:	Austin Gadient explained to me the mod method in order to code the tick marks on the grid
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity scopeFace is
    Port ( row : in  unsigned(9 downto 0);
           column : in  unsigned(9 downto 0);
		   trigger_volt: in unsigned (9 downto 0);
		   trigger_time: in unsigned (9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
          ch1: in std_logic;
          ch1_enb: in std_logic;
          ch2: in std_logic;
          ch2_enb: in std_logic);
end scopeFace;

architecture Behavioral of scopeFace is
signal grid_h: std_logic;
signal grid_v: std_logic;
signal t_level: std_logic;
signal t_time: std_logic;

begin
        -- draws the trigger level triangle 
        t_level <= '1' when (column = 21 and ((row = (trigger_volt -2)) or (row = (trigger_volt - 1)) or (row = (trigger_volt)) or (row = (trigger_volt +1 )) or (row = (trigger_volt +2)))) else
                   '1' when (column = 22 and ((row = (trigger_volt - 1)) or (row = (trigger_volt)) or (row = (trigger_volt + 1)))) else
                   '1' when (column = 23 and row = trigger_volt) else
                   '0';
        -- draws the trigger time triangle 
        t_time <= '1' when (row = 21 and ((column = (trigger_time -2)) or (column = (trigger_time - 1)) or (column = (trigger_time)) or (column = (trigger_time +1 )) or (column = (trigger_time +2)))) else
                  '1' when (row = 22 and ((column = (trigger_time - 1)) or (column = (trigger_time)) or (column = (trigger_time + 1)))) else
                  '1' when (row = 23 and column = trigger_time) else
                  '0';
        -- draws the horizontal grid lines          
        grid_h <= '0' when (column < 20 or column > 620) else
                  '1' when (row = 20) else
                  '1' when (row = 70) else
                  '1' when (row = 120) else
                  '1' when (row = 170) else
                  '1' when (row = 220) else
                  '1' when (row = 270) else
                  '1' when (row = 320) else
                  '1' when (row = 370) else
                  '1' when (row = 420) else
                  '1' when ((row = 219 or row = 221) and ( (column - 5) mod 15 = 0)) else
                  '0';
        -- draws the vertical grid lines         
        grid_v <= '0' when (row < 20 or row > 420) else
                  '1' when (column = 20) else
                  '1' when (column = 80) else
                  '1' when (column = 140) else
                  '1' when (column = 200) else
                  '1' when (column = 260) else
                  '1' when (column = 320) else
                  '1' when (column = 380) else
                  '1' when (column = 440) else
                  '1' when (column = 500) else
                  '1' when (column = 560) else
                  '1' when (column = 620) else
                  '1' when ((column = 319 or column = 321) and ( row  mod 10 = 0)) else
                  '0';
                  
         
        r <= x"FF" when(grid_h = '1') else
             x"FF" when(grid_v = '1') else
             x"FF" when(t_level = '1') else
             x"FF" when(t_time = '1') else
             x"00" when(ch2 = '1') else
             x"FF" when(ch1 = '1') else
             x"00";
             
        g <= x"FF" when(grid_h = '1') else
             x"FF" when(grid_v = '1') else
             x"FF" when(t_level = '1') else
             x"FF" when(t_time = '1') else
             x"FF" when(ch2 = '1') else
             x"FF" when(ch1 = '1') else
             x"00";
             
        b <= x"FF" when(grid_h = '1') else
             x"FF" when(grid_v = '1') else
             x"00" when(t_level = '1') else
             x"00" when(t_time = '1') else
             x"00" when(ch2 = '1') else
             x"00" when(ch1 = '1') else
             x"00";
          

end Behavioral;