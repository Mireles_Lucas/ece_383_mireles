 # Lab 3- Software control of a datapath 
 
 ## By C2C Lucas Mireles
 
 ## Table of Contents
 1. [Objectives or Purpose](#objectives-or-purpose)
 2. [Preliminary Design](#preliminary-design)
 3. [Hardware schematic](#hardware-schematic)
 4. [Debugging](#debugging)
 5. [Testing methodology or results](#testing-methodology-or-results)
 6. [Observations and Conclusions](#observations-and-conclusions)
 7. [Documentation](#documentation)
  
 ### Lab 3 Objectives and Purpose 
 In this lab, we will integrate the video display controller developed in Lab 2 with the MicroBlaze processor built using the fabric of the Artix-7 FPGA. In the preceding lectures, we learned about the Vivado and SDK tool chains, now its time to put that knowledge to the test by building a software controlled datapath. Lab 2 revealed some shortcomings of our oscilloscope that this lab intends on correcting. Specifically, we will add:
 A horizontal trigger point
 The ability to enable and disable which channels are being displayed
 The ability to trigger off of channel 2
 The ability to change the slope direction of the trigger
 
 ### Preliminary Design 
 
 The first step in designing this lab was to change the lab 2 datapath in order for the microblaze to correctly interact with our module placed inside of an ip. The lab 2 datapath can be seen in the block diagram below and how it interacts with the lab as a whole.
 
 ![](images/block_diagram.JPG)
 #####Figure 1. Block Diagram
 
 After making the required changes to lab 2 datapath, it was necessary to create an ip in Vivado named my _ oscope _ ip which would rout the correct signals going to the microblaze and those going outside of the Artix 7 chip.  Below we can see which signals need to go where.
 
 ![](images/signals.JPG)
 #####Figure 2. Signals coming to and from datapath
 In order to correctly rout the signals to communicate with the microblaze we needed to correctly assign signals coing from datapath or into the datapath using slave registers.  The following code gives an example of the lab 2 datapath instantiation inside of the my _ oscope _ ip.
 
     	datapath: lab2_datapath port map(
         clk => S_AXI_ACLK,
         reset_n => S_AXI_ARESETN,
         ac_mclk => ac_mclk,
         ac_adc_sdata => ac_adc_sdata,
         ac_dac_sdata => ac_dac_sdata,
         ac_bclk => ac_bclk,
         ac_lrclk => ac_lrclk,
         scl => scl,
         sda => sda,
         tmds => tmds,
         tmdsb => tmdsb,
         sw => sw,
         cw => cw,
         exWrAddr => slv_reg1(9 downto 0),
         exWen => slv_reg2(0),
         exSel => slv_reg3(0),
         Lbus_out => L_bus_out,
         Rbus_out => R_bus_out,
         exLbus => slv_reg8(15 downto 0),
         exRbus => slv_reg9(15 downto 0),        
         flagQ => flagQ,
         flagClear => slv_reg0(7 downto 0), -- to be able to read and write from slave register 0
         trigger_time => unsigned(slv_reg6(9 downto 0)),
         trigger_volt => unsigned(slv_reg7(9 downto 0)));
 
 After creating and successfully connecting all signals coming to and from the mircroblaze, I needed to place my newly created ip inside of my actual block design and connect it to the microblaze. The following figure illustrates the block design for the project.
 
 ![](images/micro.JPG)
 #####Figure 3. Block design for lab 3.
 
 The next and last step was to program our newly created ip with the microblaze. I completed this in SDK using c code.  The first step in programing the code was to find out which addresses to use for the slave registers. The below code demonstrates the naming and addresses of the slave registers used in the code.
 
 	#define O_Base					0x44a00000
 	
 	#define O_flagQReg				O_Base		// slv_reg0
 	#define	O_flagClearReg			O_Base		// slv_reg0
 	#define	O_exWrAddrReg			O_Base + 4	// slv_reg1
 	#define	O_exWenReg				O_Base + 8  // slv_reg2
 	#define	O_exSelReg				O_Base + 12	// slv_reg3
 	#define	O_L_bus_outReg			O_Base + 16	// slv_reg4
 	#define	O_R_bus_outReg			O_Base + 20  // slv_reg5
 	#define	O_trigger_timeReg		O_Base + 24	// slv_reg6
 	#define	O_trigger_voltReg		O_Base + 28	// slv_reg7
 	#define	O_exLbusReg				O_Base + 32  // slv_reg8
 	#define	O_exRbusReg				O_Base + 36  // slv_reg9
 
 After finding the addresses of the slave registers, I simply programmed the ip based off the ready bit of our flag register, the trigger time, and trigger voltage. Once this was completed, lab 3 functionality was complete.
 
 ### Debugging
 The biggest issue I faced during this lab was naming my signals wrong in my datapath. It took me forever to realize that I was using the exact same signal, exLbus for both the right and left channel and could not figure out why I was not getting 2 channels and when I did get to channels, they were not updating correctly. Once this naming issue was fixed, completing functionality was not that difficult. Additionally, in the beginning stages, my math was very wrong for the external bus signals because I was shifting the bits and trying to complete math on them where my actual VHDL code was taking care of the math.
 ### Testing methodology or results
 
 ##### Functionality
 In order to make required functionality, you will need to properly trigger the oscilloscope on channel 1 using a positive edge trigger. Control of this process is to be performed using the MicroBlaze. The main tasks of the MicroBlaze will include:
 
 - Move audio samples into a pair of circular buffers. These circular buffers will be maintained in the address space of the MicroBlaze. That is, you should have two big arrays defined in your program. Use polling of the ready bit of the flag register.
 - Examine the samples, looking for a trigger event.
 - Fill the remaining sample slots in memory.
 - Move the appropriate buffer values into the display memory of the oscilloscope (lab2) component.
 - Provide a user menu (through the terminal) allowing the user to adjust the trigger voltage and trigger time.
 - Use the ready bit of the flag register to trigger an interrupt. The ISR should store the samples (left and right), look for a triggering event, and signal when the stored samples should be transfered to the BRAM in the oscilloscope component.
 
 
 Below can be found the video that demonstrates required functionality for lab 3.
 
 
-[Proof of Functionality](https://www.youtube.com/watch?v=mmcKX7HTLls)
+[Proof of Functionality](https://youtu.be/1v0MMIX_GiM)
 
 
 ![](images/functionality.jpg)
 ### Figure 4. Channel 1 triggering based off of trigger volt and trigger time using an ISR
 
 
 ### Observations and Conclusions
 The objective of this lab was to use to use my existing datapath created in lab 2, make an ip that allows a microblaze to communicate with it, and program that ip succesffuly using the microblaze.  I needed to trigger data coming into the chip based off of the trigger time and trigger voltage and display the resulting signal onto a HDMI display. Based off of this, I would say that I have successfully completed lab. 
 
 I will use the experience gained from this lab in future projects like our final project in order to successfully communicate with a customized ip using a microblaze.
 ### Documentation
 None.