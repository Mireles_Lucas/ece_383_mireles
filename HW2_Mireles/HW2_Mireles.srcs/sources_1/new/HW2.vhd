----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	HW2_tb.vhd
-- HW:		HW2
-- Pupr:	Test bench for HW2  
--
-- Documentation:	Based this off a some previous labs and consulted 
--					page 36 of our text for some useful syntax.	
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity HW2 is
    Port ( D : in STD_LOGIC_VECTOR (7 downto 0);
           H : out STD_LOGIC_VECTOR (3 downto 0));
end HW2;

architecture Behavioral of HW2 is

begin

        H <= x"0" when D = x"45" else
             x"1" when D = x"16" else
             x"2" when D = x"1e" else
             x"3" when D = x"26" else
             x"4" when D = x"25" else
             x"5" when D = x"2e" else
             x"6" when D = x"36" else
             x"7" when D = x"3d" else
             x"8" when D = x"3e" else
             x"9" when D = x"46";

end Behavioral;
