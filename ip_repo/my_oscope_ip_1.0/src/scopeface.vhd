----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	scopeface.vhd
-- HW:		-
-- Pupr:	scopeface for final project modified from lab1
--
-- Documentation:	Austin Gadient explained to me the mod method in order to code the tick marks on the grid
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity scopeFace is
    Port ( row : in  unsigned(9 downto 0);
           column : in  unsigned(9 downto 0);
		   player1_horz: in unsigned (9 downto 0);
		   player1_vert: in unsigned (9 downto 0);
		   player2_horz: in unsigned (9 downto 0);
           player2_vert: in unsigned (9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
          ch1: in std_logic;
          ch1_enb: in std_logic;
          ch2: in std_logic;
          ch2_enb: in std_logic);
end scopeFace;

architecture Behavioral of scopeFace is
signal grid_h: std_logic;
signal grid_v: std_logic;
signal player1: std_logic;
signal player2: std_logic;
signal tail_1, tail_2: std_logic;
signal player1_y, player1_x : std_logic_vector( 9 downto 0);

begin
player1_y <= std_logic_vector(player1_vert);
player1_x <= std_logic_vector(player1_horz);
        -- draws the trigger level triangle 
        player1 <= '1' when (player1_vert = row + 4 or player1_vert = row + 3 or (player1_vert = row + 2) or (player1_vert = row + 1) or (player1_vert = row) or (player1_vert = row - 1) or (player1_vert = row - 2 ) or (player1_vert = row -3) or (player1_vert = row-4)) and 
                              ((player1_horz = column + 4) or (player1_horz = column + 3) or (player1_horz = column + 2) or (player1_horz = column + 1) or ( player1_horz= column) or (player1_horz = column - 1) or (player1_horz = column - 2 ) or (player1_horz = column -3) or (player1_horz = column -4))else    
                    '0';
        -- draws the trigger time triangle 
        player2 <= '1' when (player2_vert = row + 4 or player2_vert = row + 3 or (player2_vert = row + 2) or (player2_vert = row + 1) or (player2_vert = row) or (player2_vert = row - 1) or (player2_vert = row - 2 ) or (player2_vert = row -3) or (player2_vert = row-4)) and 
                   ((player2_horz = column + 4) or (player2_horz = column + 3) or (player2_horz = column + 2) or (player2_horz = column + 1) or ( player2_horz= column) or (player2_horz = column - 1) or (player2_horz = column - 2 ) or (player2_horz = column -3) or (player2_horz = column -4))else    
                     '0';
--        -- draws the horizontal grid lines          
        grid_h <= '1' when (column = 160 or column = 480 or column = 159 or column = 479) else
                  '1' when (row = 80 or row = 81 or row = 400 or row = 401) else
                  '0';
            
        tail_1 <= '1' when ch1 = '1' else
                  '0';
        tail_2 <= '1' when ch2 = '1' else
                                    '0';         
         
        r <= x"FF" when(grid_h = '1') else
--             x"FF" when(grid_v = '1') else
             x"FF" when(player1 = '1') else
             x"00" when(player2 = '1') else
             x"00" when(ch1_enb = '1') else
             x"FF" when (tail_1 = '1') else
             x"00" when (tail_2 = '1') else
             x"00";
             
        g <= x"FF" when(grid_h = '1') else
--             x"FF" when(grid_v = '1') else
             x"00" when(player1 = '1') else
             x"FF" when(player2 = '1') else
             x"00" when(ch1_enb = '1')else
             x"00" when (tail_1 = '1') else
             x"FF" when (tail_2 = '1') else
             x"00";
             
        b <= x"FF" when(grid_h = '1') else
--             x"FF" when(grid_v = '1') else
             x"00" when(player1 = '1') else
             x"93" when(player2 = '1') else
             x"00" when(ch1_enb = '1') else
             x"00" when (tail_1 = '1') else
             x"93" when (tail_2 = '1') else
             x"00";
end Behavioral;