----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	lab2_datapath.vhd
-- HW:		-
-- Pupr:	Implentation of datapath for Final project based off of lab 2
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library UNIMACRO;		-- This contains links to the Xilinx block RAM
use UNIMACRO.vcomponents.all;
--use work.lab2Parts.all;	

entity lab2_datapath is
    Port(
	clk : in  STD_LOGIC;
	ch1_enb: in std_logic;
	ja : inout STD_LOGIC_Vector(7 downto 0);
	reset_n : in  STD_LOGIC;
	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC;	
	tmds : out  STD_LOGIC_VECTOR (3 downto 0);
	tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
	player1_horz: in unsigned( 9 downto 0);--player1_horz
	player1_vert: in unsigned( 9 downto 0);
	p2horz: out unsigned( 9 downto 0);
	p2vert: out unsigned (9 downto 0);
	button_press: out std_logic); --player1_vert
end lab2_datapath;

architecture Behavioral of lab2_datapath is
component video is
    Port (clk : in  STD_LOGIC;
          reset_n : in  STD_LOGIC;
          tmds : out  STD_LOGIC_VECTOR (3 downto 0);
          tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
          player1_horz: in unsigned(9 downto 0);
          player1_vert: in unsigned (9 downto 0);
          player2_horz: in unsigned(9 downto 0);
          player2_vert: in unsigned (9 downto 0);
          row: out unsigned(9 downto 0);
          column: out unsigned(9 downto 0);
          ch1: in std_logic;
          ch1_enb: in std_logic;
          ch2: in std_logic;
          ch2_enb: in std_logic;
          v_synch: out std_logic);
end component;

component controller is
    Port ( clk : in std_logic;
           reset_n : in std_logic;
           ja : inout STD_LOGIC_Vector(7 downto 0);
           p2x: out unsigned (9 downto 0);
           p2y: out unsigned (9 downto 0);
           button_press: out std_logic);
end component;

    signal row, column: unsigned(9 downto 0);
	signal ch1_wave, ch2_wave, v_synch: std_logic := '0';
	signal p2x, p2y: unsigned (9 downto 0);
	signal reset : std_logic;
	signal RD_sig1x, WR_sig1x: std_logic_vector(9 downto 0):= "0000000000";

	signal RD_sig2x, WR_sig2x: std_logic_vector(9 downto 0):= "0000000000";
    signal DI_sig1x, DO_sig1x: std_logic_vector(31 downto 0):= x"00000000";
    signal DI_sig2x, DO_sig2x: std_logic_vector(31 downto 0):= x"00000000";
    
    signal player1_y, player1_x : std_logic_vector( 9 downto 0);
    signal player2_y, player2_x : std_logic_vector( 9 downto 0);
    signal p1x_32, p1y_32, p2x_32, p2y_32: unsigned(9 downto 0);
    
    
begin

process(clk)
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                DI_sig1x <= x"00000000";
                            elsif player1_vert = 85 then
                            DI_sig1x(0) <= '1' ;
                            elsif player1_vert = 95 then
                            DI_sig1x(1) <= '1' ;
                            elsif player1_vert = 105 then
                            DI_sig1x(2) <= '1' ;
                            elsif player1_vert = 115 then
                            DI_sig1x(3) <= '1' ;
                            elsif player1_vert = 125 then
                            DI_sig1x(4) <= '1' ;
                            elsif player1_vert = 135 then
                            DI_sig1x(5) <= '1' ;
                            elsif player1_vert = 145 then
                            DI_sig1x(6) <= '1' ;
                            elsif player1_vert = 155 then
                            DI_sig1x(7) <= '1' ;
                            elsif player1_vert = 165 then
                            DI_sig1x(8) <= '1' ;
                            elsif player1_vert = 175 then
                            DI_sig1x(9) <= '1' ;
                            elsif player1_vert = 185 then
                            DI_sig1x(10) <= '1' ;
                            elsif player1_vert = 195 then
                            DI_sig1x(11) <= '1' ;
                            elsif player1_vert = 205 then
                            DI_sig1x(12) <= '1' ;
                            elsif player1_vert = 215 then
                            DI_sig1x(13) <= '1' ;
                            elsif player1_vert = 225 then
                            DI_sig1x(14) <= '1' ;
                            elsif player1_vert = 235 then
                            DI_sig1x(15) <= '1' ;
                            elsif player1_vert = 245 then
                            DI_sig1x(16) <= '1' ;
                            elsif player1_vert = 255 then
                            DI_sig1x(17) <= '1' ;
                            elsif player1_vert = 265 then
                            DI_sig1x(18) <= '1' ;
                            elsif player1_vert = 275 then
                            DI_sig1x(19) <= '1' ;
                            elsif player1_vert = 285 then
                            DI_sig1x(20) <= '1' ;
                            elsif player1_vert = 295 then
                            DI_sig1x(21) <= '1' ;
                            elsif player1_vert = 305 then
                            DI_sig1x(22) <= '1' ;
                            elsif player1_vert = 315 then
                            DI_sig1x(23) <= '1' ;
                            elsif player1_vert = 325 then
                            DI_sig1x(24) <= '1' ;
                            elsif player1_vert = 335 then
                            DI_sig1x(25) <= '1' ;
                            elsif player1_vert = 345 then
                            DI_sig1x(26) <= '1' ;
                            elsif player1_vert = 355 then
                            DI_sig1x(27) <= '1' ;
                            elsif player1_vert = 365 then
                            DI_sig1x(28) <= '1' ;
                            elsif player1_vert = 375 then
                            DI_sig1x(29) <= '1';
                            elsif player1_vert = 385 then
                            DI_sig1x(30) <= '1' ;
                            elsif player1_vert = 395 then
                            DI_sig1x(31) <= '1' ;
                        end if;
                        end if;
                  
end process;

process(clk)
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                DI_sig2x <= x"00000000";
                            elsif p2y = 85 then
                            DI_sig2x(0) <= '1';
                            elsif p2y = 95 then
                            DI_sig2x(1) <= '1';
                            elsif p2y = 105 then
                            DI_sig2x(2) <= '1';
                            elsif p2y = 115 then
                            DI_sig2x(3) <= '1';
                            elsif p2y = 125 then
                            DI_sig2x(4) <= '1';
                            elsif p2y = 135 then
                            DI_sig2x(5) <= '1';
                            elsif p2y = 145 then
                            DI_sig2x(6) <= '1';
                            elsif p2y = 155 then
                            DI_sig2x(7) <= '1';
                            elsif p2y = 165 then
                            DI_sig2x(8) <= '1';
                            elsif p2y = 175 then
                            DI_sig2x(9) <= '1';
                            elsif p2y = 185 then
                            DI_sig2x(10) <= '1';
                            elsif p2y = 195 then
                            DI_sig2x(11) <= '1';
                            elsif p2y = 205 then
                            DI_sig2x(12) <= '1';
                            elsif p2y = 215 then
                            DI_sig2x(13) <= '1';
                            elsif p2y = 225 then
                            DI_sig2x(14) <= '1';
                            elsif p2y = 235 then
                            DI_sig2x(15) <= '1';
                            elsif p2y = 245 then
                            DI_sig2x(16) <= '1';
                            elsif p2y = 255 then
                            DI_sig2x(17) <= '1';
                            elsif p2y = 265 then
                            DI_sig2x(18) <= '1';
                            elsif p2y = 275 then
                            DI_sig2x(19) <= '1';
                            elsif p2y = 285 then
                            DI_sig2x(20) <= '1';
                            elsif p2y = 295 then
                            DI_sig2x(21) <= '1';
                            elsif p2y = 305 then
                            DI_sig2x(22) <= '1';
                            elsif p2y = 315 then
                            DI_sig2x(23) <= '1';
                            elsif p2y = 325 then
                            DI_sig2x(24) <= '1';
                            elsif p2y = 335 then
                            DI_sig2x(25) <= '1';
                            elsif p2y = 345 then
                            DI_sig2x(26) <= '1';
                            elsif p2y = 355 then
                            DI_sig2x(27) <= '1';
                            elsif p2y = 365 then
                            DI_sig2x(28) <= '1';
                            elsif p2y = 375 then
                            DI_sig2x(29) <= '1';
                            elsif p2y = 385 then
                            DI_sig2x(30) <= '1';
                            elsif p2y = 395 then
                            DI_sig2x(31) <= '1';
                        end if;
                        end if;
                  
end process;
reset <= not reset_n;

WR_sig1x <= std_logic_vector(player1_horz);
RD_sig1x <= std_logic_vector(column);

--DI_sig1y <= std_logic_vector(p1x_32);
--WR_sig1y <= std_logic_vector(player1_vert);
--RD_sig1y <= std_logic_vector(row);


WR_sig2x <= std_logic_vector(p2x);
RD_sig2x <= std_logic_vector(column);

--DI_sig2y <= std_logic_vector(p2x_32);
--WR_sig2y <= std_logic_vector(p2y);
--RD_sig2y <= std_logic_vector(row);

ch1_wave <= '1' when                            ((row = 85 and DO_sig1x(0) = '1') or 
                                                (row = 95 and DO_sig1x(1) = '1') or
                                                (row = 105 and DO_sig1x(2) = '1') or
                                                (row = 115 and DO_sig1x(3) = '1') or
                                                (row = 125 and DO_sig1x(4) = '1') or
                                                (row = 135 and DO_sig1x(5) = '1') or
                                                (row = 145 and DO_sig1x(6) = '1') or
                                                (row = 155 and DO_sig1x(7) = '1') or
                                                (row = 165 and DO_sig1x(8) = '1') or
                                                (row = 175 and DO_sig1x(9) = '1') or
                                                (row = 185 and DO_sig1x(10) = '1') or
                                                (row = 195 and DO_sig1x(11) = '1') or
                                                (row = 205 and DO_sig1x(12) = '1') or
                                                (row = 215 and DO_sig1x(13) = '1') or
                                                (row = 225 and DO_sig1x(14) = '1') or
                                                (row = 235 and DO_sig1x(15) = '1') or
                                                (row = 245 and DO_sig1x(16) = '1') or
                                                (row = 255 and DO_sig1x(17) = '1') or
                                                (row = 265 and DO_sig1x(18) = '1') or
                                                (row = 275 and DO_sig1x(19) = '1') or
                                                (row = 285 and DO_sig1x(20) = '1') or
                                                (row = 295 and DO_sig1x(21) = '1') or
                                                (row = 305 and DO_sig1x(22) = '1') or
                                                (row = 315 and DO_sig1x(23) = '1') or
                                                (row = 325 and DO_sig1x(24) = '1') or
                                                (row = 335 and DO_sig1x(25) = '1') or
                                                (row = 345 and DO_sig1x(26) = '1') or
                                                (row = 355 and DO_sig1x(27) = '1') or
                                                (row = 365 and DO_sig1x(28) = '1') or
                                                (row = 375 and DO_sig1x(29) = '1') or
                                                (row = 385 and DO_sig1x(30) = '1') or
                                                (row = 395 and DO_sig1x(31) = '1')) else '0';


                
ch2_wave <= '1' when   ((row = 85 and DO_sig2x(0) = '1') or 
                                                (row = 95 and DO_sig2x(1) = '1') or
                                                (row = 105 and DO_sig2x(2) = '1') or
                                                (row = 115 and DO_sig2x(3) = '1') or
                                                (row = 125 and DO_sig2x(4) = '1') or
                                                (row = 135 and DO_sig2x(5) = '1') or
                                                (row = 145 and DO_sig2x(6) = '1') or
                                                (row = 155 and DO_sig2x(7) = '1') or
                                                (row = 165 and DO_sig2x(8) = '1') or
                                                (row = 175 and DO_sig2x(9) = '1') or
                                                (row = 185 and DO_sig2x(10) = '1') or
                                                (row = 195 and DO_sig2x(11) = '1') or
                                                (row = 205 and DO_sig2x(12) = '1') or
                                                (row = 215 and DO_sig2x(13) = '1') or
                                                (row = 225 and DO_sig2x(14) = '1') or
                                                (row = 235 and DO_sig2x(15) = '1') or
                                                (row = 245 and DO_sig2x(16) = '1') or
                                                (row = 255 and DO_sig2x(17) = '1') or
                                                (row = 265 and DO_sig2x(18) = '1') or
                                                (row = 275 and DO_sig2x(19) = '1') or
                                                (row = 285 and DO_sig2x(20) = '1') or
                                                (row = 295 and DO_sig2x(21) = '1') or
                                                (row = 305 and DO_sig2x(22) = '1') or
                                                (row = 315 and DO_sig2x(23) = '1') or
                                                (row = 325 and DO_sig2x(24) = '1') or
                                                (row = 335 and DO_sig2x(25) = '1') or
                                                (row = 345 and DO_sig2x(26) = '1') or
                                                (row = 355 and DO_sig2x(27) = '1') or
                                                (row = 365 and DO_sig2x(28) = '1') or
                                                (row = 375 and DO_sig2x(29) = '1') or
                                                (row = 385 and DO_sig2x(30) = '1') or
                                                (row = 395 and DO_sig2x(31) = '1')) else '0';

    
	video_inst: video port map( 
		clk => clk,
		reset_n => reset_n,
		tmds => tmds,
		tmdsb => tmdsb,
		player1_horz => player1_horz,
		player1_vert => player1_vert,
		player2_horz => p2x,
        player2_vert => p2y,  
		row => row, 
		column => column,
		ch1 => ch1_wave,
		ch1_enb => ch1_enb,
		ch2 => ch2_wave,
		ch2_enb => '1',
		v_synch => v_synch); 
    
    NES: controller 
            Port Map( clk => clk ,
                   reset_n => reset_n,
                   ja => ja,
                   p2x => p2x,
                   p2y => p2y,
                   button_press => button_press);
        
		p2horz <= p2x;
		p2vert <= p2y;
    
    Player_1X_Memory: BRAM_SDP_MACRO
                        generic map (
                            BRAM_SIZE => "36Kb",                     -- Target BRAM, "18Kb" or "36Kb"
                            DEVICE => "7SERIES",                     -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
                            DO_REG => 0,                             -- Optional output register disabled
                            INIT => X"000000000000000000",            -- Initial values on output port
                            INIT_FILE => "NONE",                    -- Not sure how to initialize the RAM from a file
                            WRITE_WIDTH => 32,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                            READ_WIDTH => 32,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                            SIM_COLLISION_CHECK => "NONE",             -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
                            SRVAL => X"000000000000000000")            -- Set/Reset value for port output
                        port map (
                            DO => DO_sig1x,                        -- Output read data port, width defined by READ_WIDTH parameter
                            RDADDR => RD_sig1x,                    -- Input address, width defined by port depth
                            RDCLK => clk,                             -- 1-bit input clock
                            RST => reset,                            -- active high reset
                            RDEN => '1',                            -- read enable 
                            REGCE => '1',                            -- 1-bit input read output register enable - ignored
                            DI => DI_sig1x,                        -- Input data port, width defined by WRITE_WIDTH parameter
                            WE => "1111",                    -- since RAM is byte read, this determines high or low byte
                            WRADDR => WR_sig1x,                    -- Input write address, width defined by write port depth
                            WRCLK => clk,                            -- 1-bit input write clock
                            WREN => '1');                             -- 1-bit input write port enable
                             -- 1-bit input write port enable
                            

        Player_2X_Memory: BRAM_SDP_MACRO
                        generic map (
                            BRAM_SIZE => "36Kb",                     -- Target BRAM, "18Kb" or "36Kb"
                            DEVICE => "7SERIES",                     -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
                            DO_REG => 0,                             -- Optional output register disabled
                            INIT => X"000000000000000000",            -- Initial values on output port
                            INIT_FILE => "NONE",                    -- Not sure how to initialize the RAM from a file
                            WRITE_WIDTH => 32,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                            READ_WIDTH => 32,                         -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
                            SIM_COLLISION_CHECK => "NONE",             -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
                            SRVAL => X"000000000000000000")            -- Set/Reset value for port output
                        port map (
                            DO => DO_sig2x,                        -- Output read data port, width defined by READ_WIDTH parameter
                            RDADDR => RD_sig2x,                    -- Input address, width defined by port depth
                            RDCLK => clk,                             -- 1-bit input clock
                            RST => reset,                            -- active high reset
                            RDEN => '1',                            -- read enable 
                            REGCE => '1',                            -- 1-bit input read output register enable - ignored
                            DI => DI_sig2x,                        -- Input data port, width defined by WRITE_WIDTH parameter
                            WE => "1111",                    -- since RAM is byte read, this determines high or low byte
                            WRADDR => WR_sig2x,                    -- Input write address, width defined by write port depth
                            WRCLK => clk,                            -- 1-bit input write clock
                            WREN => '1');                             -- 1-bit input write port enable
                            -- 1-bit input write port enable
                            

        



end Behavioral;
