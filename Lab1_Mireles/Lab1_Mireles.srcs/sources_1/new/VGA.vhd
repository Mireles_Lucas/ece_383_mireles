----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	VGA.vhd
-- HW:		-
-- Pupr:	vga for lab 1
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vga is
	Port(	clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			h_sync : out  STD_LOGIC;
			v_sync : out  STD_LOGIC; 
			blank : out  STD_LOGIC;
			r: out STD_LOGIC_VECTOR(7 downto 0);
			g: out STD_LOGIC_VECTOR(7 downto 0);
			b: out STD_LOGIC_VECTOR(7 downto 0);
			trigger_time: in unsigned(9 downto 0);
			trigger_volt: in unsigned (9 downto 0);
			row: out unsigned(9 downto 0);
			column: out unsigned(9 downto 0);
			ch1: in std_logic;
			ch1_enb: in std_logic;
			ch2: in std_logic;
			ch2_enb: in std_logic);
end vga;

architecture Behavioral of VGA is

signal Column_sig : unsigned(9 downto 0);
signal Row_sig : unsigned (9 downto 0);
signal roll_sig: std_logic;
signal roll2_sig: std_logic;
signal ctrl_sig: std_logic := '1';
signal h_synch_sig: std_logic;
signal v_synch_sig: std_logic;
signal v_blank: std_logic;
signal h_blank: std_logic;


component counter_c 
    Port ( clk : in STD_LOGIC;
           reset_n : in STD_LOGIC;
           ctrl : in STD_LOGIC;
           roll : out  STD_LOGIC;
           column : out unsigned (9 downto 0));
end component;

component counter_r 
    Port ( clk : in STD_LOGIC;
           reset_n : in STD_LOGIC;
           ctrl : in STD_LOGIC;
           roll : out  STD_LOGIC;
           row : out unsigned (9 downto 0));
end component;

component scopeFace
    Port ( row : in  unsigned(9 downto 0);
           column : in  unsigned(9 downto 0);
		   trigger_volt: in unsigned (9 downto 0);
		   trigger_time: in unsigned (9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
          ch1: in std_logic;
          ch1_enb: in std_logic;
          ch2: in std_logic;
          ch2_enb: in std_logic);
end component;

begin
        Column_counter: counter_c 
        Port map( clk => clk,
               reset_n => reset_n,
               ctrl => ctrl_sig,
               roll => roll_sig,
               column => Column_sig);
        
        
        Row_counter: counter_r 
        Port map ( clk => clk,
                   reset_n => reset_n,
                   ctrl => roll_sig,
                   roll => roll2_sig,
                   row => Row_sig);
                   
        Scope: scopeFace
            Port map( row => Row_sig,
                   column => Column_sig,
                   trigger_volt => trigger_volt,
                   trigger_time => trigger_time,
                   r => r,
                   g => g,
                   b => b,
                  ch1=> ch1,
                  ch1_enb => ch1_enb,
                  ch2 => ch2,
                  ch2_enb => ch2_enb);
        
        -- implements the counters for the h_synch and h_blank signals 
        column_process: process(clk)
           begin
               if (rising_edge(clk)) then
                       
                   if (reset_n = '0') then
                       h_blank <= '1';
                       h_synch_sig <= '0'; 
                       
                   else
                           if ((Column_sig > 637) and (Column_sig < 799)) then
                                h_blank <= '1';
                           else h_blank <= '0';
                           end if;
                    
                           if ((Column_sig > 653) and (Column_sig < 750)) then
                                 h_synch_sig <= '0';
                           else h_synch_sig <= '1';
                           end if;                                           
                  end if;
               end if;
           end process;
           
    -- CSA for the v_blank and v_synch singnals 
    v_blank <= '1' when (Row_sig > 478 and Row_sig <= 524) else '0';
    v_synch_sig <= '0' when (Row_sig > 488 and Row_sig < 491) else '1';
    
    row <= Row_sig;
    column <= Column_sig;
    blank <= v_blank or h_blank;
    h_sync <= h_synch_sig;
    v_sync <= v_synch_sig;

end Behavioral;
