----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	lab1.vhd
-- HW:		-
-- Pupr:	top level for lab 1
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity lab1 is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
			  btn: in	STD_LOGIC_VECTOR(4 downto 0);
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0));
end lab1;

architecture structure of lab1 is

	signal row, column: unsigned(9 downto 0);
	signal trigger_time: unsigned (9 downto 0):= to_unsigned(200,10);
	signal trigger_volt: unsigned (9 downto 0):= to_unsigned(150,10);
	signal old_button, button_activity: std_logic_vector(4 downto 0);
	signal ch1_wave, ch2_wave: std_logic;
	
	component video is
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
			  trigger_time: in unsigned(9 downto 0);
			  trigger_volt: in unsigned (9 downto 0);
			  row: out unsigned(9 downto 0);
			  column: out unsigned(9 downto 0);
			  ch1: in std_logic;
			  ch1_enb: in std_logic;
			  ch2: in std_logic;
			  ch2_enb: in std_logic);
	end component;

begin


	------------------------------------------------------------------------------
	-- the variable button_activity will contain a '1' in any position which 
	-- has been pressed or released.  The buttons are all nominally 0
	-- and equal to 1 when pressed.
	------------------------------------------------------------------------------
    process(clk)
    begin
        if(rising_edge(clk)) then
            if (reset_n = '0') then
                button_activity <= "00000";
                old_button <= "00000";
            else
                button_activity <= (btn xor old_button) and btn;
                old_button <= btn;
                
           end if;
         end if;
     end process;

	------------------------------------------------------------------------------
	-- If a button has been pressed then increment of decrement the trigger vars
	------------------------------------------------------------------------------
	process(clk)
	begin
	       if(rising_edge(clk)) then
	           if (button_activity(0) = '1' and row > 20) then
	               trigger_volt <= trigger_volt - 5;
	           elsif (button_activity(2) = '1' and row < 420) then
	               trigger_volt <= trigger_volt + 5;
	           elsif (button_activity(1) = '1' and column > 20) then
                   trigger_time <= trigger_time - 5;
               elsif (button_activity(3) = '1' and column < 620) then
                   trigger_time <= trigger_time + 5;
               end if;
             end if;
     end process;
	           
	           
	------------------------------------------------------------------------------
	------------------------------------------------------------------------------
	
	video_inst: video port map( 
		clk => clk,
		reset_n => reset_n,
		tmds => tmds,
		tmdsb => tmdsb,
		trigger_time => trigger_time,
		trigger_volt => trigger_volt,
		row => row, 
		column => column,
		ch1 => ch1_wave,
		ch1_enb => '1',
		ch2 => ch2_wave,
		ch2_enb => '1'); 
		
    -- draws the green and yellow lines that form an x
    ch1_wave <= '1' when (row = column) and row >= 20 and row <= 420 and column >= 20 and column <= 620 else '0';
    ch2_wave <= '1' when (row = (440 - column)) and row >= 20 and row <= 420 and column >= 20 and column <= 620 else '0';
	
end structure;
