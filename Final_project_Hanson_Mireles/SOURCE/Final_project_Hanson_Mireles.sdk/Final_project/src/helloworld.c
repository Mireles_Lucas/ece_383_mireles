/*--------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	March 7, 2016
-- File:	helloworld.c
-- Event:	Final Project
-- Crs:		ECE 383
--
-- Purp:	handles all of the game logic begind the game play. Also controls the direct inputs for player 1 via the WASD keys.
--
-- Documentation:	none
--
-- Academic Integrity Statement: I certify that, while others may have
-- assisted me in brain storming, debugging and validating this program,
-- the program itself is my own work. I understand that submitting code
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to
-- another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/***************************** Include Files ********************************/

#include "xparameters.h"
#include "stdio.h"
#include "xstatus.h"

#include "platform.h"
#include "xil_printf.h"						// Contains xil_printf
#include <xuartlite_l.h>					// Contains XUartLite_RecvByte
#include <xil_io.h>							// Contains Xil_Out8 and its variations
#include <xil_exception.h>

/************************** Constant Definitions ****************************/

/*
 * The following constants define the slave registers used for Lab 3
 */
#define O_Base				0x44a00000
#define Channel_Enable		O_Base		//slv_reg0
#define Button_press		O_Base + 4	//slv_reg1
#define Player2_horzReg		O_Base + 16
#define Player2_vertReg		O_Base + 20
#define	Player1_horzReg		O_Base + 24	// slv_reg6
#define	Player1_vertReg		O_Base + 28	// slv_reg7

#define	uartRegAddr			0x40600000		// read <= RX, write => TX

#define printf xil_printf			/* A smaller footprint printf */
/************************** Function Prototypes ****************************/
void myISR(void);

/************************** Variable Definitions **************************/
/*
 * The following are declared globally so they are zeroed and so they are
 * easily accessible from a debugger
 */

u16 player1_horz = 15;  // starts the player 1 position
u16 player1_vert = 16;
u16 player1_array[32][32]; // array for the position of player 1

u16 player2_horz;
u16 player2_vert;
u16 player2_array[32][32]; //array for player 2

int game_over = 0;
int player = 0;


int main(void){
		unsigned char c;

		init_platform();
		for (int k = 0; k <5000; k++){
			Xil_Out16(Channel_Enable, 1);
		}

		Xil_Out16(Player1_horzReg, (player1_horz*10) + 165);
		Xil_Out16(Player1_vertReg, (player1_vert*10) + 85);

	    microblaze_register_handler((XInterruptHandler) myISR, (void *) 0);
	    microblaze_enable_interrupts();


		print("Welcome to Lu LightCycles!!!\n\r");
		printf("--------------------------\r\n");

		printf("	It is Player 1's turn! Good luck and Don't Die!!\r\n");


		printf("	Player1 x  = %d\r\n",Xil_In16(Player1_horzReg));
		printf("	Player1 y= %d\r\n",Xil_In16(Player1_vertReg));
		printf("	Player2 x  = %d\r\n",Xil_In16(Player2_horzReg));
		printf("	Player2 y= %d\r\n",Xil_In16(Player2_vertReg));

		Xil_Out16(Channel_Enable, 0);
		while (game_over == 0){


//					if (Xil_In16(Button_press) == 1){



								player2_horz = Xil_In16(Player2_horzReg);
								player2_vert = Xil_In16(Player2_horzReg);



								if (player1_array[(player2_horz - 165) / 10][(player2_vert - 85) / 10] == 1 ){ //if player 2 has been where hes been before ot player 1
									game_over = 1;
									player = 1; //end the game
								}
								else if(player2_horz>= 500 || player2_horz<= 140 || player2_vert >= 400 || player2_vert <= 80){
									game_over = 1;
									player = 1;
								}
								else{ // if not, store in the array that home boy has been there
									player2_horz = (Xil_In16(Player2_horzReg)-165) / 10;
									player2_vert = (Xil_In16(Player2_horzReg)-85) /10;

									player2_array[player2_horz][player2_vert] = 1;

								}


			if (XUartLite_IsReceiveEmpty(uartRegAddr)){
					}


			else{

				c=XUartLite_RecvByte(uartRegAddr);
					switch(c){
					case'w': //move up

							player1_vert -= 1;
							if (player1_array[player1_horz][player1_vert] == 1 || player2_array[player1_horz][player1_vert] == 1){ //if player 1 has been where hes been before
								game_over = 1;
								player = 2; //end the game
							}
							else if((player1_vert*10) + 85 >= 400 || (player1_vert*10) + 85 <= 80){
								game_over = 1;
								player = 2;
							}
							else{ // if not, store in the array that home boy has been there
								player1_array[player1_horz][player1_vert] = 1;
							}


							Xil_Out16(Player1_vertReg,(player1_vert*10) + 85);

							break;
//
					case's': // move down
							player1_vert += 1;
							if (player1_array[player1_horz][player1_vert] == 1 || player2_array[player1_horz][player1_vert] == 1){ //if player 1 has been where hes been before
								game_over = 1; //end the game
								player = 2;
							}
							else if((player1_vert*10) + 85 >= 400 || (player1_vert*10) + 85 <= 80){
								game_over = 1;
								player = 2;
							}
							else{ // if not, store in the array that home boy has been there
								player1_array[player1_horz][player1_vert] = 1;
							}

							Xil_Out16(Player1_vertReg, (player1_vert*10) + 85);
							break;
					case'a': //move left
							player1_horz -= 1;
							if (player1_array[player1_horz][player1_vert] == 1 || player2_array[player1_horz][player1_vert] == 1){ //if player 1 has been where hes been before
								game_over = 1; //end the game
								player = 2;
							}
							else if((player1_horz*10) + 165 >= 500 || (player1_horz*10) + 165 <= 140){
								game_over = 1;
								player = 2;
							}

							else{ // if not, store in the array that home boy has been there
								player1_array[player1_horz][player1_vert] = 1;
							}

							Xil_Out16(Player1_horzReg, (player1_horz*10) + 165);
							break;
					case'd'://move right
							player1_horz += 1;

							if (player1_array[player1_horz][player1_vert] == 1 || player2_array[player1_horz][player1_vert] == 1){ //if player 1 has been where hes been before
								game_over = 1; //end the game
								player = 2;
							}
							else if((player1_horz*10) + 165 >= 500 || (player1_horz*10) + 165 <= 140){
								game_over = 1;
								player = 2;
							}
							else{ // if not, store in the array that home boy has been there
								player1_array[player1_horz][player1_vert] = 1;
							}

							Xil_Out16(Player1_horzReg, (player1_horz*10) + 165);
							break;

							}



			}


	}
		if (player == 2){
			printf(" GAMEOVER: Player 2 has won!");

		}
		else{
			printf(" GAMEOVER: Player 1 has won!");
		}
		cleanup_platform();
		return 0;


		}


void myISR(void){
	return;
}
