----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	HW3_tb.vhd
-- HW:		HW3
-- Pupr:	testbench for HW3
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity HW3_tb is
end HW3_tb;

architecture Behavioral of HW3_tb is

COMPONENT HW3 is 
    port( DIP : in unsigned (7 downto 0);
           LED : out STD_LOGIC);
end COMPONENT;

signal DIPsig: unsigned (7 downto 0);
signal LEDsig: std_logic;
begin

	----------------------------------------------------------------------
	-- Create an instance of your majority
	----------------------------------------------------------------------
	UUT:	HW3 port map (DIPsig, LEDsig);

	tb : PROCESS
	BEGIN
	
		-----------------------------------------
		-- Parse out the bits of the test_vector
		-----------------------------------------
		DIPsig <= to_unsigned(17,8);
		    wait for 10 ns; 
		    assert LEDsig = '1'
 			report "Error with case 1" severity warning;
	   
	   DIPsig <= to_unsigned(0,8);
            wait for 10 ns; 
            assert LEDsig = '0'
            report "Error with case 2" severity warning;
                                  
        DIPsig <= to_unsigned(34,8);
            wait for 10 ns; 
            assert LEDsig = '1'
            report "Error with case 3" severity warning;
            
        DIPsig <= to_unsigned(199,8);
            wait for 10 ns; 
            assert LEDsig = '0'
            report "Error with case 4" severity warning;
            
	---------------------------
	-- Just halt the simulator
	---------------------------
	assert TRUE = FALSE 
		report "---------------Self-checking testbench completed.  Nominal circuit behavior---------------"
		severity failure;
			
	END PROCESS tb;

end architecture Behavioral;
