----------------------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Spring 2017
-- Course: 	ECE 383
-- File: 	HW3.vhd
-- HW:		HW3
-- Pupr:	code for HW3
--
-- Documentation:	None
-- 	
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity HW3 is
    Port ( DIP : in unsigned (7 downto 0);
           LED : out STD_LOGIC);
end HW3;

architecture Behavioral of HW3 is

begin
    LED <=  '1' when DIP = to_unsigned(17,8) else
            '1' when DIP = to_unsigned(34,8) else
            '1' when DIP = to_unsigned(51,8) else
            '1' when DIP = to_unsigned(68,8) else
            '1' when DIP = to_unsigned(85,8) else
            '1' when DIP = to_unsigned(102,8) else
            '1' when DIP = to_unsigned(119,8) else
            '1' when DIP = to_unsigned(136,8) else
            '1' when DIP = to_unsigned(153,8) else
            '1' when DIP = to_unsigned(170,8) else
            '1' when DIP = to_unsigned(187,8) else
            '1' when DIP = to_unsigned(204,8) else
            '1' when DIP = to_unsigned(221,8) else
            '1' when DIP = to_unsigned(238,8) else
            '1' when DIP = to_unsigned(255,8) else
            '0';
            
            
end Behavioral;
