--------------------------------------------------------------------
-- Name:	Lucas Mireles
-- Date:	Feb 3, 2017
-- File:	lec11_cu.vhdl
-- HW:	Lecture 11
--	Crs:	ECE 383
--
-- Purp:	Most of the control unit for the keyboard scan circuit
--
-- Documentation:	I pulled some information from chapter .
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lec11_cu is
Port(	    clk: in  STD_LOGIC;
			reset : in  STD_LOGIC;
			kbClk: in std_logic;
			cw: out STD_LOGIC_VECTOR(2 downto 0);
			sw: in STD_LOGIC;
			busy: out std_logic);
end lec11_cu;

architecture Behavioral of lec11_cu is

    type state_type is (Init, Receive, Clk_high, Shift_data, Clk_low);
    signal state: state_type := Init;
begin

    state_process: process(clk)
	 begin
		if (rising_edge(clk)) then
			if (reset = '0') then 
				state <= Init;
			else
				case state is
					when Init =>
					   busy <= '0';
					   cw <= "011";
					   if (kbClk = '0') then state <= Receive; end if;
				    when Receive =>
				        busy <= '1';
				        cw <= "000";
				        state <= Clk_high;
				    when Clk_high =>
				        cw <= "000";
				    
				        if (kbClk = '0' and sw = '0') then
				            state <= Shift_data;
				            end if;
				    when Shift_data =>
				        cw <= "101";
				        state <= Clk_low;
				    when Clk_low =>
				        cw <= "000";
				        if (sw = '1') then 
                           state <= Init;
				        elsif (kbClk = '1') then state <= Clk_high; end if;
				end case;
			end if;
		end if;
	end process;
end Behavioral;
